#!/usr/bin/python

__version_info__ = ('1', '2', '0')
__version__ = '.'.join(__version_info__)

#############################################################################
# Phase 0. Initialization
# Doing stuff like parsing arguments, and reading the files.
#
from dataconversion import DataSet, PY_INDENT, thisyear
import pandas as pd
from datetime import datetime
import textwrap
import os

# Small utility to create hyperlink to hyperlink :-)
def print_as_link(text):
    return f'[{text}]({text})'

# These are the files we work with
# Create list
dataset = DataSet(
    opening='(c) ' + thisyear
            + ' Biometris, Wageningen University and Research.',
    description='Converts the EFSA CAPEG database Excel sheet into MCRA '
                + 'effects and assessment groups.',
    epilog='For example: use %(prog)s -v -x for a verbose example.',
    directory='Build/Output',
    version = __version__)
#
#
dataset.add(
    name='capeg',
    short_argument='-c',
    help='The (input) EFSA CAPEG file - '
         + 'format: xls (Excel).',
    default_name='capeg_20210706_13492613.xls',
    default_dir='Input',
    direction='Input')
#
# The output files
dataset.add(
    name='effects',
    short_argument='-e',
    help='The (output) effects file - '
         + 'format: csv (Comma Seperated).',
    default_name='Effects.csv',
    default_dir='Build/Output')
#
dataset.add(
    name='agmm',
    short_argument='-m',
    help='The (output) assessment group membership models file - '
         + 'format: csv (Comma Seperated).',
    default_name='AssessmentGroupMembershipModels.csv',
    default_dir='Build/Output')
#
dataset.add(
    name='agm',
    short_argument='-a',
    help='The (output) assessment group membership file - '
         + 'format: csv (Comma Seperated).',
    default_name='AssessmentGroupMemberships.csv',
    default_dir='Build/Output')
#
dataset.add(
    name='substances',
    short_argument='-s',
    help='The (output) substances file - '
         + 'format: csv (Comma Seperated).',
    default_name='Substances.csv',
    default_dir='Build/Output')
#

#############################################################################

dataset.init()
# To abbreviate
capeg = dataset.capeg.sheet
# We need to clean up the table firstly
# Remove all CasNumbers with na
# capeg.drop(capeg.loc[capeg['casNumber'] == 'na'].index, inplace=True)

# Change all CasNumbers na into 7440-50-8
capeg['casNumber'].replace('na', '7440-50-8', inplace=True)

# Remove all obscure Casnumers
# capeg.drop(capeg.loc[capeg['casNumber']
# .str.contains('[\(\)/]', regex=True)].index, inplace=True)

# Handle every obscure case separate, just to be explicit.
# First case: 824-39-5/26498-36-2
# beceomes 824-39-5 and 26498-36-2
capeg = capeg.mcra.dup_reggroups('casNumber', '([0-9\-]*)\s?/\s?([0-9\-]*)')
# Second case: 468-44-0 + 510-75-8 (mixture 8030-53-3)
# becomes 468-44-0 and 510-75-8
capeg = capeg.mcra.dup_reggroups('casNumber', '([0-9\-]*)\s?\+\s?([0-9\-]*)')
# Third case: 71751-41-2 (65195-55-3 B1a, 65195-56-4 B1b)
# becomes 65195-55-3, 65195-56-4
capeg = capeg.mcra.dup_reggroups('casNumber', '\(([0-9\-]*).*\,\s+([0-9\-]*)')
# Fourth case: 8018-01-7 (formerly 8065-67-6)
# becomes 8018-01-7
capeg = capeg.mcra.dup_reggroups('casNumber', '^([0-9\-]*)')
# Max length of strings (second argument)
max_len = slice(0,99)

# FIRST The effects table
# Add the fields for the effects table
capeg.mcra.addcolumn(
    {'idEffect', 'Name', 'Description', 'Reference', 'targetL1', 'AcuteChronic'})
# Create extra colum for proper CAS1 names
capeg['targetL1'] = capeg['target_CAG1'].str.split().str[0].str.strip()
capeg['targetL1'].replace('Bones', 'Skeleton', inplace=True)
capeg['targetL1'].replace('Bone', 'Bone marrow', inplace=True)

# Create tempcopy
capeg2 = capeg.copy(deep=True)
# Fill the idEffectA and idEffectC eg. L1-Liver-Acute
capeg['AcuteChronic'] = 'Acute'
capeg['idEffect'] = 'L1-' + \
    capeg['targetL1'].str.split().str[0].str.strip() + '-' + \
        capeg['AcuteChronic']
capeg2['AcuteChronic'] = 'Chronic'
capeg2['idEffect'] = 'L1-' + \
    capeg2['targetL1'].str.split().str[0].str.strip() + '-' + \
        capeg2['AcuteChronic']
# Description
capeg['Description'] = capeg['AcuteChronic'] + ' adverse effects on ' + \
    capeg['targetL1'].str.lower() + '.'
capeg2['Description'] = capeg2['AcuteChronic'] + ' adverse effects on ' + \
    capeg2['targetL1'].str.lower() + '.'
# Combine the sheets, append the second after the first
capeg = capeg.append(capeg2, ignore_index=True)
# Set the name
capeg['Name'] = capeg['targetL1'].str[max_len]
# Set the reference
capeg['Reference'] = ''

# Remove Acute rows without ARfD
capeg.drop(capeg.loc[(capeg['AcuteChronic'] == 'Acute') & ((capeg['arfd'] == 'na') | capeg['arfd'].isna())].index, inplace=True)

# Remove Chronic rows without ADI
capeg.drop(capeg.loc[(capeg['AcuteChronic'] == 'Chronic') & ((capeg['adi'].isna() | capeg['arfd'].isna()))].index, inplace=True)

#capeg.to_excel('dump.xlsx', sheet_name='Dump', index=False)

# Done, now wrap this table up
effects_header = ['idEffect', 'CodeSystem', 'Name', 'Description',
                  'BiologicalOrganisation', 'KeyEventProcess',
                  'KeyEventObject', 'KeyEventAction', 'KeyEventOrgan',
                  'KeyEventCell', 'AOPwikiKE', 'Reference']

dataset.effects.sheet = capeg.drop_duplicates(
    subset=['idEffect'], ignore_index=True)[
        ['idEffect', 'Name', 'Description', 'Reference']]

dataset.effects.sheet.mcra.keepcolumn(effects_header)

# SECOND The Assessment group membership models table
# Remove and add used columns to clear them
capeg.drop(['Name', 'Description', 'Reference'], axis=1, inplace=True)
capeg.mcra.addcolumn(['id', 'Name', 'Description', 'Reference'])

# ID
capeg['id'] = 'AG1-' + \
    capeg['idEffect'].str.split('-').str[1:].str.join('-')
# Name
capeg['Name'] = ('AG Level 1 ' + capeg['targetL1'].str.title() + \
    ' ' + capeg['AcuteChronic']).str[max_len]
# Description
capeg['Description'] = \
    'Cummulative assesment group for ' + capeg['AcuteChronic'].str.lower() + \
        ' adverse effects on ' + capeg['targetL1'].str.lower() + '.'
# Reference
capeg['Reference'] = 'https://doi.org/10.2903/sp.efsa.2012.EN-269'

# Done, now wrap this table up
agmm_header = ['id', 'Name', 'Description', 'idEffect', 'Accuracy',
               'Sensitivity', 'Specificity', 'Reference']

dataset.agmm.sheet = capeg.drop_duplicates(
    subset=['id'], ignore_index=True)[
        ['id', 'idEffect', 'Name', 'Description', 'Reference']]

dataset.agmm.sheet.mcra.keepcolumn(agmm_header)

# THIRD The Substances table
# Remove and add used columns to clear them
capeg.drop(['Name', 'Description', 'Reference'], axis=1, inplace=True)
capeg.mcra.addcolumn(['idSubstance', 'Name', 'Description'])

# ID
capeg['idSubstance'] = capeg['casNumber']
# Name
capeg['Name'] = capeg['activeSubstance'].str[max_len]

# Done, now wrap this table up
substances_header = ['idSubstance', 'Name', 'Description',
                     'ConcentrationUnit', 'CramerClass', 'MolecularMass']

dataset.substances.sheet = capeg.drop_duplicates(
    subset=['idSubstance'], ignore_index=True)[
        ['idSubstance', 'Name']]

dataset.substances.sheet.mcra.keepcolumn(substances_header)

# FOURTH The Assessment group memberships table
# Remove and add used columns to clear them
capeg.drop(['Name', 'Description'], axis=1, inplace=True)
capeg.mcra.addcolumn(['idGroupMembershipModel', 'GroupMembership'])

# ID
capeg['idGroupMembershipModel'] = capeg['id']
capeg['GroupMembership'] = '1'

# Done, now wrap this table up
agm_header = ['idGroupMembershipModel', 'idSubstance', 'GroupMembership']

dataset.agm.sheet = capeg[agm_header].drop_duplicates()

dataset.agm.sheet.mcra.keepcolumn(agm_header)

# DONE
dataset.close(file_report=True)
