## Introduction

This script creates an MCRA complient dataset from the data from the Cumulative Assessment Groups of Pesticides as proposed by Nielsen et al. 2012. It uses the data from the [CAPEG 1.2 database](https://efsa.onlinelibrary.wiley.com/action/downloadSupplement?doi=10.2903%2Fsp.efsa.2012.EN-269&file=269eax2-sup-0002.zip) from the supplementary material as source data and generates MCRA complient tables containing the assessment group definitions, and also including a substances catalogue and effects catalogue.

These are the input and output files of the script. All names are defaults, and can be changed by the user on the command line.

Elsa Nielsen, Pia Nørhede, Julie Boberg, Louise Krag Isling, Stine Kroghsbo, Niels Hadrup, Lea Bredsdorff, Alicja Mortensen, John Christian Larsen, 2012. Identification of Cumulative Assessment Groups of Pesticides. EFSA Supporting Publication 2012; 9( 4):EN-269, 303 pp. doi:10.2903/sp.efsa.2012.EN-269

# How to use the convert script

## Quick start

### Install required packages
```
pip install -r requirements.txt
```

### Run the script in trial (-x) and verbose (-v) mode
```
python.exe Convert-DTUCAG.py -x -v
```

### Run the script with specific input files
```
python.exe Convert-DTUCAG.py -v -e MyEffects.csv -s MySubstances.csv
```

### Questions?
```
python.exe Convert-DTUCAG.py -h
```

## Coding

If you would like to add code, please try and stick to the [Python Code Guidelines](https://www.python.org/dev/peps/pep-0008/).
Check your changes using ``pycodestyle`` for example.

```
pip install pycodestyle  # To install the programm
pycodestyle .\Convert-DTUCAG.py  # To check whether the code complies.
```
