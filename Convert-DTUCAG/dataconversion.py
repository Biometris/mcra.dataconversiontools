from argparse import ArgumentParser, SUPPRESS
import pandas as pd
import xlrd
from datetime import datetime
from urllib.parse import urlparse
import os  # path, mkdir, walk
import time  # ctime
import types
import uuid
import zipfile
import requests
import hashlib
import numpy as np
import math
import sys
import textwrap
import getpass
import re

__version_info__ = ('0', '9', '2')
__version__ = '.'.join(__version_info__)

# For debugging purposes
# from objbrowser import browse

PY_INDENT = '    '
thisyear = datetime.now().strftime('%Y')


@pd.api.extensions.register_dataframe_accessor('mcra')
class McraAccessor:
    '''
    This is an extension of the panda object model.
    Some often used functions are added here.
    '''
    def __init__(self, pandas_obj):
        self._obj = pandas_obj

        def here_concat(*args):
            '''
            To easily join two columns
            '''
            strs = [str(arg) for arg in args if not pd.isnull(arg)]
            return '-'.join(strs) if strs else np.nan

        self.concat = np.vectorize(here_concat)

    def copycolumn(self, columnnames):
        '''
        To easily copy a bunch of columns
        '''
        for fromcol, tocol in columnnames.items():
            self._obj[tocol] = self._obj[fromcol]

    def addcolumn(self, columnnames):
        '''
        To easily add a bunch of empty columns
        '''
        for col in columnnames:
            if col not in self._obj.columns:
                self._obj[col] = ''

    def keepcolumn(self, columnnames):
        '''
        To easily format to fixed set of columns
        '''
        # Add missing ones, making them empty
        for col in columnnames:
            if col not in self._obj.columns:
                self._obj[col] = ''
        # Only retain the ones given.
        self._obj = self._obj[columnnames]

    def splitjoin(self, name, split, join,
                  split_sep='-', right_split=True, join_sep='-'):
        '''
        Splits a column, and then joins the result with another column
        '''
        # Due to the SettingWithCopyWarning we do it a bit cumbersome
        df = pd.DataFrame()
        df[join] = self._obj[join]
        if right_split:
            df[split] = self._obj[split].str.rsplit(split_sep, n=1).str[1]
        else:
            df[split] = self._obj[split].str.rsplit(split_sep, n=1).str[0]

        df[name] = df.loc[:, (join, split)].apply(
                    lambda x: join_sep.join(x.dropna()), axis=1)
        df = df.drop([join, split], axis=1)
        # Not ideal yet, but slightly better than it used to be....
        self._obj = self._obj.merge(df, left_index=True, right_index=True)
        return self._obj

    def join(self, name, join_left, join_right, sep='-'):
        '''
        joins with another column
        '''
        # Due to the SettingWithCopyWarning we do it a bit cumbersome
        df = pd.DataFrame()
        df[[join_left, join_right]] = self._obj[[join_left, join_right]]
        df[name] = df.loc[:, (join_left, join_right)].apply(
                    lambda x: sep.join(x.dropna()), axis=1)
        df = df.drop([join_left, join_right], axis=1)
        # Not ideal yet, but slightly better than it used to be....
        self._obj = self._obj.merge(df, left_index=True, right_index=True)
        return self._obj

    def dump(self, filename):
        '''
        For debugging purposes, to dump a file from memory a bit more easily
        '''
        base, ext = os.path.splitext(filename)
        print('Dump file  : {file}.'.format(file=filename))
        if ext == '.csv':
            self._obj.to_csv(path_or_buf=filename, index=False)
        elif ext == '.tsv':
            self._obj.to_csv(path_or_buf=filename, index=False, sep='\t')
        elif ext == '.xlsx':
            self._obj.to_excel(filename, sheet_name='Dump', index=False)

    def dup_reggroups(self, column, regex):
        temp_col = column+'__temp__'
        dups=self._obj[column].str.extractall(regex)
        dups[temp_col]=dups.values.tolist()
        dups=dups.reset_index(level=[1])
        self._obj=self._obj.join(dups[temp_col]).explode(temp_col).reset_index(drop=True)
        self._obj.loc[(self._obj[temp_col].notna()),column]=self._obj[temp_col]
        self._obj.drop(columns=temp_col, inplace=True)
        return self._obj


class DataFile:
    '''
    A class to work with the files more streamlined.
    Contains technical details just to use the files in a simple manner.

    :param default_name: The default name for the file, can also
    determine the output name/sheet.
    :param default_dir: The default directory in which to place the file
    :param checksum: If given, a file can be checked/reused
    :param necessary: Whether the file is necessary or not.
    '''
    def __init__(self, default_name, default_dir, necessary=True):
        self.default_name = default_name
        self.default_base = os.path.splitext(self.default_name)[0]
        self.default_dir = default_dir
        self.path = None
        self.directory = None
        self.reportpath = None
        self.zippath = None
        self.suggested = None
        self.exist = False
        self.modified = ''
        self.extension = None
        self.size = 0
        self.size_string = ''
        self.hash = ''
        self.hash_short = ''
        self.checksum = None
        self.necessary = necessary

    def update(self):
        '''
        Updates file properties, e.g. for output files.
        '''
        if os.path.exists(self.path) and os.path.isfile(self.path):
            self.exist = True
            self.modified = time.ctime(os.path.getmtime(self.path))
            self.size = os.path.getsize(self.path)
            self.size_string = self.__converttoprefix(self.size)
            self.hash = str(self.__md5_hash())
            self.hash_short = self.hash[0:8]

    def __converttoprefix(self, bytes):
        '''
        Private function to have some nice formatting of filesizes
        '''
        if bytes <= 1024:
            return '{0:.0f}B'.format(bytes)
        else:
            power = math.floor(math.log(bytes, 1024))
            factor = math.pow(1024, power)
            prefix = ['B', 'K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y']
            if round(bytes/factor, 1) < 10:
                return '{0:.1f}{prefix}'.format(
                    bytes/factor, prefix=prefix[power])
            elif round(bytes/factor, 1) >= 1000:
                return '{0:.2f}{prefix}'.format(
                    bytes/(1024*factor), prefix=prefix[power+1])
            else:
                return '{0:.0f}{prefix}'.format(
                    bytes/factor, prefix=prefix[power])

    def __md5_hash(self):
        '''
        Returns an MD5 hash of the file; file will be processed
        '''
        md5_hash = hashlib.md5()
        with open(self.path, "rb") as f:
            # Read and update hash in chunks of 4K
            for byte_block in iter(lambda: f.read(4096), b""):
                md5_hash.update(byte_block)
        return md5_hash.hexdigest()

    def suggest(self, name, force_dir=None):
        '''
        This is the filename the user suggests on the command line.
        It has to be changed (perhaps) to a proper path
        e.g. if the user only gave a directory
        '''
        if self.suggested is None:
            self.suggested = name
        else:
            print('double assignment for '+name)

        if self.suggested is None:
            self.path = os.path.join(
                self.default_dir, self.default_name)
        else:
            if urlparse(self.suggested).netloc:
                urlbase, urlfilename = os.path.split(
                    urlparse(self.suggested).path)
                self.path = os.path.join(
                    self.default_dir, urlfilename)
            else:
                head, tail = os.path.split(self.suggested)
                if os.path.isdir(self.suggested):
                    self.path = os.path.join(
                        self.suggested, self.default_name)
                elif tail == self.suggested:
                    self.path = os.path.join(
                        self.default_dir, self.suggested)
                else:
                    self.path = self.suggested
        head, tail = os.path.split(self.path)
        if force_dir is not None:
            head = force_dir
        self.path = os.path.join(head, tail)
        self.directory = head
        base, ext = os.path.splitext(self.path)
        self.reportpath = base+'.md'
        self.extension = ext
        self.update()


class DataSheet:
    '''
    This is a container for file properties and the pandas sheet.
    :param default_name: The default name of the file
    :param default_dir: The default directory of the file
    :param checksum:
    '''
    def __init__(self, file, checksum=None, direction='Output',
                 autoload=True):
        self.file = file
        self.sheet = None
        self.checksum = checksum
        self.properties = ''
        self.report = ''
        self.direction = direction
        self.autoload = autoload
        self.closed = False

    def get_report(self):
        report = ''
        if self.direction == 'Input' or \
                (self.direction == 'Output' and self.closed):
            filename = os.path.split(self.file.path)[1]
            report += f'* {self.direction} file: [{filename}]({filename})\n'
            report += textwrap.indent(
                '* {props}\n'.format(props=self.properties), PY_INDENT)
            report += textwrap.indent(
                '* Modified: {mod}\n'.format(mod=self.file.modified),
                PY_INDENT)
            report += textwrap.indent(
                '* File size: {size_str} ({size} B)\n'.format(
                    size_str=self.file.size_string,
                    size=self.file.size), PY_INDENT)
            report += textwrap.indent(
                '* Hash: {hash}\n'.format(hash=self.file.hash), PY_INDENT)
        return report

    def update_properties(self):
        if self.sheet is not None:
            shape = '[{rows} rows x {columns} columns]'.format(
                rows=str(self.sheet.shape[0]),
                columns=str(self.sheet.shape[1]))
            self.properties = \
                'Format: {sh}; filesize: {fs}; hash: {h}.'.format(
                    sh=shape, fs=self.file.size_string,
                    h=self.file.hash_short)

    @staticmethod
    def supply_defaults(default, **kwargs):
        '''
        If not in the arguments, these will be defaults.
        '''
        for key, values in default.items():
            if key not in kwargs:
                kwargs[key] = values
        return kwargs

    def load(self, **kwargs):
        if self.file.exist:
            if self.file.extension == '.csv':
                kwargs = self.supply_defaults(
                    {'comment': '#', 'dtype': str}, **kwargs)
                self.sheet = pd.read_csv(self.file.path, **kwargs)
            elif self.file.extension == '.tsv':
                kwargs = self.supply_defaults(
                        {'comment': '#', 'dtype': str, 'sep': '\t'}, **kwargs)
                self.sheet = pd.read_csv(self.file.path, **kwargs)
            elif self.file.extension == '.xlsx':
                self.sheet = pd.read_excel(self.file.path, **kwargs)
            elif self.file.extension == '.xls':
                # Suppress warnings
                wb = xlrd.open_workbook(self.file.path,
                                        logfile=open(os.devnull, 'w'))
                self.sheet = pd.read_excel(wb, engine='xlrd')
            elif self.file.extension == '.md':
                f = open(self.file.path, 'r')
                self.sheet = f.read()
                f.close()
            else:
                # Error here
                print(' COULD NOT READ {file}- unknown extenstion.'.format(
                    file=self.file.path))
            self.update_properties()

    def save(self, **kwargs):
        if self.file.extension == '.csv':
            kwargs = self.supply_defaults(
                {'index': False}, **kwargs)
            self.sheet.to_csv(
                path_or_buf=self.file.path,
                **kwargs)
        elif self.file.extension == '.tsv':
            kwargs = self.supply_defaults(
                {'index': False, 'sep': '\t'}, **kwargs)
            self.sheet.to_csv(
                path_or_buf=self.file.path,
                **kwargs)
        elif self.file.extension == '.xlsx':
            kwargs = self.supply_defaults(
                {'index': False}, **kwargs)
            self.sheet.to_excel(
                self.file.path,
                sheet_name=self.file.default_base,
                **kwargs)
        else:
            print(' COULD NOT WRITE {file} - unknown extenstion.'.format(
                file=self.file.path))
        self.update_properties()

    def close(self, header=False, auto_report=False, also_save=True):
        '''
        If auto_report is False, no automatic report will be made.
        '''
        if header:
            # Make a sheet with the specified header in that order
            self.sheet.mcra.addcolumn(header)
            self.sheet = self.sheet[header]
        if also_save:
            self.save()
        self.closed = True
        self.file.update()
        self.update_properties()
        if auto_report:
            self.report += self.get_report()
        # We are no longer creating an md-report per file
        # if len(self.report) > 0:
        #     # Save report
        #     with open(self.file.reportpath, 'w+') as f:
        #         f.write(self.report)
        if '-v' in sys.argv or '--verbosity' in sys.argv:
            print(f'Output file: {self.file.path}; {self.properties}')


class DataSet:
    def __init__(self, opening=None, description=None,
                 epilog=None, directory=None, version=False):
        self.args = None
        self.list = []
        # Whether or not to create a zip file
        self.zip = None
        # The report for the entire dataset
        self.report = ''
        self.runtime = datetime.now().strftime('%H:%M:%S, %d %b %Y')
        self.runcommand = ' '.join(sys.argv)
        self.runarguments = ' '.join(sys.argv[1:])
        self.usedarguments = None
        self.scriptname = os.path.split(sys.argv[0])[1]
        md5_hash = hashlib.md5()
        with open(sys.argv[0], "rb") as f:
            # Read and update hash in chunks of 4K
            for byte_block in iter(lambda: f.read(4096), b""):
                md5_hash.update(byte_block)
        self.scripthash = md5_hash.hexdigest()
        m=re.match('(.*)\-(?P<noun>.*)\.py', self.scriptname)
        if m:
            self.scriptnoun = m.group('noun')
        else:
            self.scriptnoun = self.scriptname.replace('.py','')
        self.runuser = getpass.getuser()

        self.parser = ArgumentParser(
            description=description, epilog=epilog)
        if directory:
            report= os.path.join(directory, "Report.md")
        else:
            report = 'Output\\Report.md'
        self.parser.add_argument(
            '-r', '--report', nargs='?',
            const=report,
            default=report,
            help='Creates a report file (default: %(const)s).')
        # The verbosity argument will accept: -v, or -vv, -vvv etc.
        # Set default to 1, so that basic output will always appear.
        self.parser.add_argument(
            '-v', '--verbosity', help="Show verbose output",
            action="count", default=0)
        # self.parser.add_argument(
        #     '-x', '--example', action='store_const', const='Example',
        #     help='Uses input files from the %(const)s subdir.')
        if version:
            self.version = version
        else:
            self.version = __version__
        zip = f'Build\\{self.scriptnoun}.{version}.zip'
        self.parser.add_argument(
            '-z', '--zip', nargs='?', const=zip, default=zip,
            help='Creates a zip file %(const)s containing all output.' +
            ' (default: %(const)s).')
        if '-v' in sys.argv or '--verbosity' in sys.argv:
            print(opening)

    # It is usefull to be able to iterate over all the datasheets.
    # Basically, avoid using .list. in all DataSet references.
    def __iter__(self):
        self.n = 0
        return self

    def __next__(self):
        if self.n < len(self.list):
            self.n = self.n + 1
            return getattr(self, self.list[self.n-1])
        else:
            raise StopIteration

    # Container for all the files
    def add(self, name, default_name, default_dir, short_argument=None,
            checksum=None, help=SUPPRESS, direction='Output', autoload=True,
            necessary=True):
        if getattr(self, name, None) is None:
            # Create a new sheet with a different name
            # directly under this class. Then no .list is needed
            setattr(self,
                    name,
                    DataSheet(
                        file=DataFile(
                            default_name=default_name,
                            default_dir=default_dir,
                            necessary=necessary),
                        direction=direction,
                        checksum=checksum,
                        autoload=autoload))
            # But we must do some bookkeeping
            self.list.append(name)

            long_argument = '--' + name + '_file'
            if type(help) == str and help is not SUPPRESS:
                help = help + ' (default: {default})'.format(
                    default=default_name)
            if short_argument is None:
                self.parser.add_argument(
                    long_argument, nargs='?',
                    const=getattr(self, name).file.default_name,
                    help=help)
            else:
                self.parser.add_argument(
                    short_argument, long_argument, nargs='?',
                    const=getattr(self, name).file.default_name,
                    help=help)
            if checksum is not None:
                self.parser.add_argument(
                    '--'+name+'_checksum',
                    default=checksum,
                    help=SUPPRESS)

    def __download(self, url, file):
        self.verbose(
            1, 'Downloading: {url} to {file}'.format(url=url, file=file))
        myfile = requests.get(url, allow_redirects=True)
        with open(file, 'wb') as download:
            download.write(myfile.content)

    def verbose(self, level, message):
        if self.args.verbosity >= level:
            print(message)

    def init(self):
        # Initializes the command line parameters
        self.args = self.parser.parse_args()
        self.usedarguments = self.args.__dict__

        for datasetname in self.list:
            dataset = getattr(self, datasetname)
            if getattr(self.args, datasetname+'_file') is None:
                datasetfilename = dataset.file.default_name
            else:
                datasetfilename = getattr(self.args, datasetname+'_file')
            self.usedarguments[datasetname+'_file'] = datasetfilename

            if dataset.direction == 'Input':
                dataset.file.suggest(
                    datasetfilename)
                if urlparse(dataset.file.suggested).netloc:
                    if (not dataset.file.exist) \
                       or ((dataset.checksum is not None)
                       and dataset.file.hash != dataset.checksum):
                        self.__download(
                            url=dataset.file.suggested,
                            file=dataset.file.path)
                dataset.file.update()
                if dataset.file.exist:
                    if dataset.checksum is not None \
                       and dataset.file.hash != dataset.checksum:
                        print('File {file} has improper checksum'.format(
                            file=dataset.file.path))
                else:
                    if dataset.file.necessary:
                        print('File {file} not found.'.format(
                            file=dataset.file.path))

                if dataset.autoload:
                    if getattr(self.args, datasetname+'_file') is None \
                       and not dataset.file.necessary:
                        # Don't load files which are not necessary and not
                        # explictly called from command line.
                        self.verbose(3, 'Not loading {file}.'.format(
                            file=dataset.file.path))
                    else:
                        dataset.load()
                        if dataset.file.exist:
                            self.verbose(
                                1,
                                'Input file : {file}; {props}'.format(
                                    file=dataset.file.path,
                                    props=dataset.properties))
                            # High verbosity, dump data.
                            # self.verbose(3, dataset.sheet)
            else:
                # It is an Ouput file
                base, ext = os.path.splitext(datasetfilename)
                dataset.file.suggest(datasetfilename)
                dataset.update_properties()

                os.makedirs(os.path.dirname(
                    os.path.abspath(dataset.file.path)), exist_ok=True)


        # Make sure we can create the report
        os.makedirs(os.path.dirname(
            os.path.abspath(self.args.report)), exist_ok=True)

        # Always create a zip file containing everything
        # First make sure the directory exists
        zippath = os.path.dirname(os.path.abspath(self.args.zip))
        os.makedirs(zippath, exist_ok=True)
        basezip, extzip = os.path.splitext(self.args.zip)
        # Setting self.zip indicates creating a zip file
        self.zip = basezip+'.zip'

    def save(self):
        for data in self:
            if data.direction == 'Output':
                if not data.closed:
                    data.close(auto_report=False, also_save=True)

    def close(self, file_report=False, save=True):
        '''
        Method to close the dataset.
        Most importantly save files.
        '''
        report_content = ''
        if file_report:
            report_content += f'* Script: {self.scriptname}\n'
            report_content += textwrap.indent(
                f'* Command line: {self.runcommand}\n', PY_INDENT)
            report_content += textwrap.indent(
                f'* Filename: {self.scriptname}\n', PY_INDENT)
            report_content += textwrap.indent(
                f'* Command line Arguments: {self.runarguments}\n', PY_INDENT)
            report_content += textwrap.indent(
                '* Arguments executed:\n', PY_INDENT)
            for key, value in self.usedarguments.items():
                if value is None:
                    report_content += textwrap.indent(
                        f'* --{key}\n', 2*PY_INDENT)
                else:
                    report_content += textwrap.indent(
                        f'* --{key} {value}\n', 2*PY_INDENT)
            report_content += textwrap.indent(
                f'* Hash: {self.scripthash}\n', PY_INDENT)
            report_content += textwrap.indent(
                f'* Executed at: {self.runtime}\n', PY_INDENT)
            report_content += textwrap.indent(
                f'* Executed by: {self.runuser}\n', PY_INDENT)
            report_content += textwrap.indent(
                f'* Version: {self.version}\n', PY_INDENT)
            report_content += textwrap.indent(
                f'* Depends upon module: {__name__}\n', PY_INDENT)
            report_content += textwrap.indent(
                f'* With version: {__version__}\n', 2*PY_INDENT)
            for data in self:
                if data.direction == 'Input':
                    report_content += data.get_report()
        # Closing every sheet first
        for data in self:
            if data.direction == 'Output':
                if not data.closed:
                    data.close(auto_report=file_report, also_save=save)

        if self.args.report:
            # Collect reports per sheet.
            for data in self:
                report_content += data.report
            if len(report_content) > 0:
                # Report contains information.
                if len(self.report) > 0:
                    self.report += '\n'
                self.report += report_content
            if len(self.report) > 0:
                # Save report
                with open(self.args.report, 'w+') as f:
                    f.write(self.report)
                self.verbose(
                    1,
                    'Output file: {file}, containing report on output.'.format(
                        file=self.args.report))

        if self.zip:
            # All output files will be added to the zip file
            zip = zipfile.ZipFile(self.zip, 'w')
            for data in self:
                if data.direction == 'Output':
                    filename = os.path.split(data.file.path)[1]
                    zip.write(data.file.path, filename)
            if self.args.report:
                filename = os.path.split(self.args.report)[1]
                zip.write(self.args.report, filename)
            zip.close()
            self.verbose(
                1,
                'Output file: {file}, containing all output.'.format(
                    file=self.zip))
