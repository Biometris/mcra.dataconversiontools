# How to use the convert script

## Quick start

### Solve all dependencies
```
pip install -r requirements.txt
```

### Run the script
It is a unix like script: running without verbose parameters (-v) gives no output if all goes well. This might be counterintuitive: in that case always use -v.

The most simple form is
```
python.exe Compile-EUProcessingFactorsDB_MCRA.py
```
A bit less simple is using SubstanceTranslations (variant Johannes)
```
python.exe Compile-EUProcessingFactorsDB_MCRA.py -r -v -z -s .\Input\SubstanceTranslations.csv
```
The most complex form is (variant Waldo)
```
python.exe Compile-EUProcessingFactorsDB_MCRA.py -r -v -z -s .\Input\SubstanceTranslations.csv -t .\Input\ProcessingTypes.csv -q .\Input\ProcTypeTranslations.csv -f .\Input\FoodTranslations.csv -g .\Input\FoodCompositions.xlsx  -m .\Output\Mismatches.xlsx
```

### Questions?
Use the help command to see which files are used for what.
```
python.exe Compile-EUProcessingFactorsDB_MCRA.py -h
```

## Introduction

This script takes data from the [EU Processing Factors file](https://zenodo.org/record/1488653/files/EU_Processing_Factors_db_P.xlsx.xlsx?download=1) and combines this with two (user supplied) files (a food translations file and a processing translations file) to get to an MCRA processing factors file with food codes and processing type codes in the desired coding system. In this way, data from the EU processing factors file can be used in MCRA analyses.

These are the input and output files of the script. All names are defaults, and can be changed by the user on the command line.

* Input files:
  * The [EU Processing Factors file](https://zenodo.org/record/1488653/files/EU_Processing_Factors_db_P.xlsx.xlsx?download=1)
  * The processing translation input file, [ProcTypeTranslations.csv](ProcTypeTranslations.csv)
  * The food translation input file, [FoodTranslations.csv](FoodTranslations.csv)
  * The processing types input file to augment info in the report, [ProcessingTypes.csv](ProcessingTypes.csv) (not used in any data processing)
  * An optional substances sheet (-s), to augment the output with the ``CASNumber``.
  * An optional FoodComposition file (-g), to augment the output with A-codes.
* Output files:
  * The goal of this script, the file [ProcessingTypes.csv](ProcessingTypes.csv) with the new MCRA ProcessingTypes. By default this file will be contained in a zip file [ProcessingFactors.zip](ProcessingFactors.zip)
  * A small markdown report is also created, usally called [Report.md](Report.md), but within the zip file is called Readme.md.
  * A csv file with a summary (and counts) of *the remaining data* of the EU sheet, called [Mismatches.csv](Mismatches.csv).

The script is a combination of a request by Waldo and Johannes.
This is what happens in the Waldo variant.
* The script wil try to match the first column (``FromFC``) of [ProcTypeTranslations.csv](ProcTypeTranslations.csv) to the column ``KeyFacets Code`` of the EU sheet. If a match is found, then the second column (``FCToProcType``) of [ProcTypeTranslations.csv](ProcTypeTranslations.csv) will become the field ``idProcessingType``.
* Then the script will try to match both the ``FromFX`` and ``FXToRpc`` column of [FoodTranslations.csv](FoodTranslations.csv) with the columns ``Matrix FoodEx2 Code`` and ``Matrix Code`` from the EU sheet, *for all rows that didn't already match in the previous step*. If a match was found, then the value of ``FXToProcType`` will be copied to ``idProcessingType``.
* If no substance file was given, then just copy the field ``ParamCode Active Substance`` to ``idSubstance``. But if a substance was given, then strip the dash from the ``CASNumber`` column in the substance file, and match the column ``ParamCode Active Substance`` in the EFSA sheet to ``code`` in the substances sheet. If a match was found then copy the modified (without dash) ``CASNumber`` to ``idSubstance``.
* If a foodcompositions file was given, then an additional translation is done. This table needs to have the layout of the MCRA FoodComposition.
  * Only records of ``idToFood`` starting with ``P`` and ``idFromFood`` which contain a dash (-) will be used
  * The ``idFromFood`` column is split on the dash (-)
  * A new column is temporarily added combining ``idToFood`` and the right part of the split on ``idFromFood``
  * For all matches of the new column with the field ``idFoodProcessed`` in ``ProcessingFactors``, the field ``idFoodProcessed`` will be replaced by the field ``idFromFood`` from the FoodComposition table, and duplicates will also be added
* Finally the output file [ProcessingFactors.csv](ProcessingFactors.csv) (contained within [ProcessingFactors.zip](ProcessingFactors.zip)) will be written, together with some reports.

## Prerequisites

In order to use the python script, the libraries required are listed in the file [requirements.txt](requirements.txt).

Install all the libraries at once with
```
pip install -r requirements.txt
```

## Coding

If you would like to add code, please try and stick to the [Python Code Guidelines](https://www.python.org/dev/peps/pep-0008/).
Check your changes using ``pycodestyle`` for example.

```
pip install pycodestyle  # To install the programm
pycodestyle .\Compile-EUProcessingFactorsDB_MCRA.py  # To check whether the code complies.
```
