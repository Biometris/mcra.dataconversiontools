#
import getpass
import WebApiClient
#
from argparse import ArgumentParser, SUPPRESS
# Please try to use the keyring
# Do not store passwords (even defaults) in a script
import keyring

# input parameters

parser = ArgumentParser(
    description=
        'This is a tool to download files from the EFSA Catalogue server',
    epilog=
        'For example Download-EFSA.py -v -u Primary -k EFSA'
)

parser.add_argument(
    '-v', '--verbosity', help="Show verbose output",
    action="count", default=0)

parser.add_argument(
    '-u', '--username', default=None,
    help='The username for the keyring.')

parser.add_argument(
    '-k', '--keyring', default=None,
    help='The keyring system to use.')

parser.add_argument(
    '-f', '--file', default='output.xml',
    help='The output file to write to.')

args = parser.parse_args()

if args.username is None:
    username = 'dummy'
else:
    username = args.username

if args.keyring is None:
    password=getpass.getpass("EFSA Ocp-Apim-Subscription-Key: ").strip()
else:
    password = keyring.get_password(args.keyring, username)

api = WebApiClient.Efsa(
    username=username,
    password=password)

# This one is rather a small file
body = {
"ExportCatalogueFile":
  {
   "catalogueCode" : "GENDER",
   "catalogueVersion" : "2.4",
   "exportType" : "catalogFullDefinition",
   "group" : "",
   "dcCode" : "",
   "fileType" : "XML"
  }
}

# This one is rather large (52 MB)
# body = {
# "ExportCatalogueFile":
#   {
#    "catalogueCode" : "PARAM",
#    "catalogueVersion" : "11.3",
#    "exportType" : "catalogFullDefinition",
#    "group" : "",
#    "dcCode" : "",
#    "fileType" : "XML"
#   }
# }

# Store in XML tree
xmltree = api.get_cataloguefile(body)
root = xmltree.getroot()
if args.verbosity >=1:
    print(
        'Retrieved catalogue, with root tag <{tag}>. Saving to {file}.'.format(
            tag=str(root.tag), file=args.file
        ))

xmltree.write(args.file, encoding='ISO-8859-1', method='xml')

