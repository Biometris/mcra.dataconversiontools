# How to use the download scripts

## Quick start

### Solve all dependencies
```
pip install keyring requests requests_toolbelt
```

### Store your credentials in the Windows Credential store
#### For MCRA
  * MCRA in the example below is a name to choose freely (keyring name)
  * Username is your MCRA username with which you connect
  * Enter your password at the prompt
```
keyring set MCRA username
```
#### EFSA
  * EFSA in the example below is a name to choose freely (keyring name)
  * Username also unimportant, lets make it Primary (store your primary key)
  * Password is your Ocp-Apim-Subscription-Key (see your profile at their [website](https://openapi-portal.efsa.europa.eu/))
```
keyring set EFSA Primary
```

### View your MCRA Repositories, files and Workspaces
```
python.exe python Download-MCRA.py -v -u username -k MCRA
```
(Change MCRA and username to the same name you have chosen when setting the keyring name)

### Download EFSA FileCatalogue
```
python .\Download-EFSA.py -v -u Primary -k EFSA
```
(Change EFSA and Primary to the same name you have chosen when setting the keyring name)

Open the Download-EFSA file, and change the body variable to request a different catalogue.


### Questions?
```
python.exe script-name -h
```

## Introduction

This is still work in progress.