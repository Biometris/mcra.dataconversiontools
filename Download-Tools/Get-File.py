#
import getpass
import WebApiClient
#
from argparse import ArgumentParser, SUPPRESS
# Please try to use the keyring
# Do not store passwords (even defaults) in a script
import keyring

# input parameters

default_mcra_url = 'https://mcra.test.wur.nl/Mcra91'

parser = ArgumentParser(
    description=
        'This is a tool to query the MCRA server',
    epilog=
        'For example %(prog)s -v -u hansheuvel -k Euromix'
)

parser.add_argument(
    '-v', '--verbosity', help="Show verbose output",
    action="count", default=0)

parser.add_argument(
    '-a', '--apiurl', default=default_mcra_url,
    help='The MCRA API URL (default: %(default)s)')

parser.add_argument(
    '-u', '--username', default=None,
    help='The username to connect to API.')

parser.add_argument(
    '-k', '--keyring', default=None,
    help='The keyring system to use.')

parser.add_argument(
    '-i', '--id', default=None,
    help='The ID of the file.')

parser.add_argument(
    '-f', '--filename', default=None,
    help='The name of the file.')

args = parser.parse_args()

if args.verbosity >= 1:
    print("URL of EuroMix API application: "+default_mcra_url+".")

if args.username is None:
    print("MCRA user name:(%s)" % getpass.getuser())
    mcrausername = input().strip() or getpass.getuser()
else:
    mcrausername = args.username

if args.keyring is None:
    mcrapassword = getpass.getpass("MCRA Password:").strip()
else:
    mcrapassword = keyring.get_password(args.keyring, mcrausername)

if args.id is None:
    print("Data source version id:")
    datasourceid = input().strip()
else:
    datasourceid = args.id

# This is the call to set-up the api
api = WebApiClient.Mcra(
    url=args.apiurl,
    username=mcrausername,
    password=mcrapassword)

if args.verbosity >= 1:
    print('Downloading {file}'.format(file=args.filename))

api.get_file(id=datasourceid, filename=args.filename)