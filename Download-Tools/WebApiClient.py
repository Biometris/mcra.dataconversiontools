#
import keyring
import requests
from xml.etree import ElementTree
from requests_toolbelt.multipart import decoder
from requests.exceptions import HTTPError
import datetime
#


class Mcra:
    # Class to talk to MCRA.

    def __init__(self, url, username, password=None, keyring=None):
        self.url = url
        self.username = username
        self.client_id = 'cbd00dfbdd0a4501bf457b52a635353f'
        # We will be reusing a token
        self.token = None
        # Make sure the token expired 10 mins ago
        self.token_expires = datetime.datetime.now() \
            + datetime.timedelta(seconds=-600)
        # If password is explicitly given, use that one
        if password is not None:
            self.password = password
        # But it is better to use the keyring!
        elif keyring is not None:
            self.password = keyring.get_password(self.keyring, self.username)
        else:
            raise Exception('No password or keyring given.')

    def get_repository(self, id=None):
        if id is None:
            result = self.__call_api__(
                apiCall='Repositories/GetAll')
        else:
            result = self.__call_api__(
                apiCall=f'Repositories/Get/{id}')
        # Making sure we can always iterate.
        if result.status_code == 500:
            # Something went wrong. No results.
            return None
        else:
            if type(result.json()) is not list:
                return [result.json()]
            else:
                return result.json()

    def get_datasource(self, id=None):
        if id is None:
            result = self.__call_api__(
                apiCall=f'DataSources/GetAll').json()
        else:
            result = self.__call_api__(
                apiCall=f'DataSources/Get/{id}').json()
        # Making sure we can always iterate.
        if type(result) is not list:
            return [result]
        else:
            return result

    def get_datasource_version(self, id=None):
        result = self.__call_api__(
            apiCall=f'DataSources/GetVersion/{id}').json()
        return result

    def get_workspace(self, id=None):
        if id is None:
            result = self.__call_api__(
                apiCall=f'Workspace/GetAll').json()
        else:
            result = self.__call_api__(
                apiCall=f'Workspace/Get/{id}').json()
        # Making sure we can always iterate.
        if type(result) is not list:
            return [result]
        else:
            return result


    def get_file(self, id, filename, csv=False):
        if csv:
            result = self.__call_api__(
                apiCall=f'DataSources/DownloadVersionCsv/{id}')
        else:
            result = self.__call_api__(
                apiCall=f'DataSources/DownloadVersion/{id}')

        if filename is None:
            dsinfo = self.get_datasource_version(id)
            filename = dsinfo['name']

        with open(filename, 'wb') as fd:
            for chunk in result.iter_content(chunk_size=128):
                fd.write(chunk)

    def __get_token__(self):
        tokenUrl = self.url + "/jwtauth/token"
        payload = {
            "grant_type": "password",
            "username": self.username,
            "password": self.password,
            "client_id": self.client_id
        }

        try:
            r = requests.post(tokenUrl, data=payload)
            r.raise_for_status()

        except HTTPError as http_err:
            print(f'HTTP error occurred: {http_err}')
        except Exception as err:
            print(f'Other error occurred: {err}')

        self.token = r.json()
        self.token['headers'] = {
            'authorization': 'bearer '+self.token['access_token'],
            'content-type': 'application/json'}
        # Use safety margin of ten minutes (600 sec)
        self.token_expires = datetime.datetime.now() \
            + datetime.timedelta(seconds=self.token['expires_in']-600)

    def __call_api__(self, apiCall):
        # Only requests token if necessary
        if self.token_expires < datetime.datetime.now():
            self.__get_token__()

        # api function to call
        pUrl = f'{self.url}/Api/{apiCall}'

        try:
            r = requests.get(pUrl, headers=self.token['headers'])
            r.raise_for_status()

        except HTTPError as http_err:
            if str(http_err).startswith('500'):
                pass
            else:
                print(f'HTTP error occurred: {http_err}')
            # In case of error 500 with the result:
            # {'message': 'An error has occurred.', 'exceptionMessage': 'Object reference not set to an instance of an object.', 'exceptionType': 'System.NullReferenceException'
            # Apparently no objects can be returned
        except Exception as err:
            print(f'Other error occurred: {err}')
        return r


class Efsa:
    # Class to talk to EFSA repositories

    def __init__(self, url='https://openapi.efsa.europa.eu',
                 username=None, password=None, keyring=None):
        # The function is call is made identical to the MCRA one.
        # Notice, that for Efsa only an Ocp-Apim-Subscription-Key is needed.
        # This key has to be stored in either the password, or as a password
        # in the keyring. The username will only be used for retrieving
        # the key in the keyring, not the actual authentication.
        self.url = url
        self.username = username
        # If password is explicitly given, use that
        if password is not None:
            self.password = password
        # But it is better to use the keyring!
        elif keyring is not None:
            self.password = keyring.get_password(self.keyring, self.username)
        else:
            raise Exception('No password or keyring given.')

        self.headers = {
            # Request headers
            'Content-Type': 'application/json',
            'Ocp-Apim-Subscription-Key': self.password,
        }

    def __call_api__(self, apiCall, body):
        # api function to call
        pUrl = f'{self.url}/api/{apiCall}'

        try:
            r = requests.post(pUrl, headers=self.headers, json=body)
            r.raise_for_status()

        except HTTPError as http_err:
            print(f'HTTP error occurred: {http_err}')
        except Exception as err:
            print(f'Other error occurred: {err}')

        # Returns the content of the attachment
        return r

    def get_cataloguefile(self, body):
        # Requests a catalogue file.
        # See https://openapi-portal.efsa.europa.eu/
        # docs/services/catalogues.REST/operations/5aafa415c0c61299adefd1f4
        # Example use:
        # body = {
        #     "ExportCatalogueFile":
        #     {
        #     "catalogueCode" : "GENDER",
        #     "catalogueVersion" : "2.4",
        #     "exportType" : "catalogFullDefinition",
        #     "group" : "",
        #     "dcCode" : "",
        #     "fileType" : "XML"
        #     }
        # }
        # Returns an ElementTree object

        apiCall = 'catalogues/catalogue-file'
        tree = ElementTree.ElementTree()
        result = self.__call_api__(
                 apiCall=apiCall,
                 body=body)
        # From the result we need the attachment
        xmlstring = decoder.MultipartDecoder(
            result.content,
            result.headers['Content-Type']).parts[1].text

        root = ElementTree.fromstring(xmlstring)
        tree._setroot(root)
        # We add a comment for future reference.
        comment = ElementTree.Comment(
            'file downloaded on {now}\n from {url}'.format(
                now=datetime.now().strftime('%H:%M:%S, %d %b %Y'),
                url=self.url+'/api/'+apiCall
            ))
        root.append(comment)
        return tree
