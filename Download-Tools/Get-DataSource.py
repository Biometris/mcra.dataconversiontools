#
import getpass
import WebApiClient
import datetime
#
from argparse import ArgumentParser, SUPPRESS
# Please try to use the keyring
# Do not store passwords (even defaults) in a script
import keyring

# input parameters

default_mcra_url = 'https://mcra.test.wur.nl/Mcra91'

parser = ArgumentParser(
    description=
        'This is a tool to query the MCRA server',
    epilog=
        'For example %(prog)s -v -u hansheuvel -k Euromix'
)

parser.add_argument(
    '-v', '--verbosity', help="Show verbose output",
    action="count", default=0)

parser.add_argument(
    '-a', '--apiurl', default=default_mcra_url,
    help='The MCRA API URL (default: %(default)s)')

parser.add_argument(
    '-u', '--username', default=None,
    help='The username to connect to API.')

parser.add_argument(
    '-k', '--keyring', default=None,
    help='The keyring system to use.')

parser.add_argument(
    '-i', '--id', default=None,
    help='The ID of the datasource.')

args = parser.parse_args()

if args.verbosity >= 1:
    print("URL of EuroMix API application: "+default_mcra_url+".")

if args.username is None:
    print("MCRA user name:(%s)" % getpass.getuser())
    mcrausername = input().strip() or getpass.getuser()
else:
    mcrausername = args.username

if args.keyring is None:
    mcrapassword=getpass.getpass("MCRA Password:").strip()
else:
    mcrapassword = keyring.get_password(args.keyring, mcrausername)

# This is the call to set-up the api
api = WebApiClient.Mcra(
    url=args.apiurl,
    username=mcrausername,
    password=mcrapassword)

print()
# Create a layout comparable to the Powershell one
print('{:<5} {:>6.6} {:<12.12} {:>20.20} {:>7.7} {:>6.6} {:>6.6} {:>7.7} {:<50.50}'.format(
    'Mode','Id','Owner','Created','DataSrc','Id','CurId','Version','Name'))
print('{:<5} {:>6.6} {:<12.12} {:>20.20} {:>7.7} {:>6.6} {:>6.6} {:>7.7} {:<50.50}'.format(
    '----','--','-----','-------','-------','--','-----','-------','----'))
# Here the actual api call is executed
result = api.get_datasource(args.id)
if result:
    print(result[0])
    for x in result:
        date=datetime.datetime.strptime(x['createdTimeStamp'], '%Y-%m-%dT%H:%M:%S.%f')
        print('{:<5} {:>6.6} {:<12.12} {:>20.20} {:>7.7} {:>6.6} {:>6.6} {:>7.7} {:<50.50}'.format(
        str(x['accessLevel'])+' '+str(int(x['isDeleted'])) \
            +' '+str(int(x['isLocalFile'])),
        str(x['idOwner']), x['owner'], datetime.datetime.strftime(date,'%d-%m-%Y    %H:%M'),
        str(x['idDataSourceRepository']), str(x['id']), str(x['idCurrentVersion']),
        str(x['version']), x['name']))
    print()