# How to use the MCRA scripts

## Quick start

### Load Module
Open a **powershell** and load the directory with the module. No backslash at the end!
```
Import-Module -Path .\MCRA.Tools
```
Perhaps put this in your profile? Do ``notepad $profile``.

### Store credential
You have to supply your username and password for the MCRA site.
Choose a TargetName to your liking. This is a name to which this credential is referred.
```
Write-MCRACredential -TargetName MCRA9 -Credential (Get-Credential)
```

### Browse your repositories
```
Get-MCRARepository -Url 'https://mcra.test.wur.nl/Mcra90' -Keyring MCRA9
```
For more convenience, store the defaults (perhaps in your profile; do ``notepad $profile``)
```
$PSDefaultParameterValues = @{
    'Get-MCRARepository:Url' = 'https://mcra.test.wur.nl/Mcra90'
    'Get-MCRARepository:Keyring' = 'MCRA9'
    'Get-MCRADataSource:Url' = 'https://mcra.test.wur.nl/Mcra90'
    'Get-MCRADataSource:Keyring' = 'MCRA9'
    'Get-MCRAWorkspace:Url' = 'https://mcra.test.wur.nl/Mcra90'
    'Get-MCRAWorkspace:Keyring' = 'MCRA9'
    'Get-MCRAFile:Url' = 'https://mcra.test.wur.nl/Mcra90'
    'Get-MCRAFile:Keyring' = 'MCRA9'}
```
Happy Powershelling:
```
Get-MCRARepository
Get-MCRAWorkspace|Where Id -eq 87
Get-MCRADataSource|Where Name -match 'agg'|Get-MCRAFile
```
### Which commands are available
```
Get-Command -Module MCRA.Tools
 ```
### Questions
```
Get-Help Get-MCRARepository -Full
```
### Examples
```
Get-Help Get-MCRARepository -Example
```

# Introduction

## Breakdown of the commands
The commands are essentially a wrapper around the standard Powershell commands.
Say, we want to execute an API call to MCRA, let's take: 'DataSources/GetAll'.
In powershell the following then needs to be executed.
```
# Get token first. Don't change the next line.
$Body = @{ 'grant_type' = 'password' }
# Change the next lines with your own credentials.
$Body['username'] = 'yourusername'
$Body['password'] =  'yourpassword'
$Body['client_id'] = 'yourclient_id'
# Use the proper URL for the website.
$Url='https://mcra.test.wur.nl/Mcra90'
$token=Invoke-RestMethod -Uri "$Url/jwtauth/token" -ContentType 'application/json' -Body $body -Method POST
# Create a proper header for future API calls
$headers = @{ 'authorization' = 'bearer '+$token.access_token }
# Now fetch the actual request from the API.
Invoke-RestMethod -Uri "$Url/Api/DataSources/GetAll" -ContentType 'application/json' -Headers $headers
# Repeat the request with this header until token expires.
```
