Function Get-DataSource {
<#
.SYNOPSIS
Retrieves an MCRA Datasource.

.DESCRIPTION
Retrieves an MCRA Datasource.

.PARAMETER CREDENTIAL
A Credential object.

.PARAMETER Keyring
The name from the entry in the Windows Credential Manager.

.PARAMETER URL
The URL to which the request is made.

.PARAMETER ID
The ID of the repository

.EXAMPLE
Get-DataSource -Url 'https://mcra.test.wur.nl/Mcra90' -TargetName MCRA


.INPUTS
System.String

.NOTES
    Naam:           Get-DataSource
    Auteur:         Heuve081
    Bron:
    Wijzigingen:    11-03-2020  Heuve081     1.0     Script geboren.

#>

    #
    # (c) Private use
    # Pas het versienummer hieronder en hierboven aan als je iets wijzigt.
    # Geef bij .NOTES hierboven aan wat je precies hebt gewijzigd.
    #
    # Pas het Outputtype aan als je een object teruggeeft.
    [OutputType('MCRA.Datasource')]
    [CmdletBinding(SupportsShouldProcess = $True)]
    Param(
        [Parameter(Mandatory=$true)]
        [string]$Url,
        [Parameter(Mandatory=$true,ParameterSetName='Credential')]
        [System.Management.Automation.PSCredential]$Credential,
        [Parameter(Mandatory=$true,ParameterSetName='TargetName')]
        [Alias('Keyring')]
        [string]$TargetName,
        [int]$ID
    )
    BEGIN   {
        # Strict modus
        Set-StrictMode -Version Latest
        # Een versienummer van het script.
        Set-Variable versie -option Constant -value '1.0' -WhatIf:$false
        # Schrijf een melding op het scherm indien nodig (bij -Verbose)
        $scriptproperty=Write-CmdLetStartMessage -Invoke $MyInvocation -Version $versie

        $tokenparams= $params = @{ 'Url' = $Url }
        if ($PSCmdlet.ParameterSetName -eq 'TargetName') {
            $tokenparams['Credential'] = Get-Credential -TargetName $TargetName
        } else {
            $tokenparams['Credential'] = $Credential
        }

        $token=Get-Token @tokenparams

        if (-not [bool]$token ) {
            Throw "  [ERROR] No token received. Mission abort."
        }

        $ApiUrl = $Url+'/Api/'
    } # Einde BEGIN
    PROCESS {
        if ($ID) {
            # This is a different API call; we expect the same objects.
            $Api='DataSources/Get/'+$ID
        } else {
            # This is a different API call; we expect the same objects.
            $Api='DataSources/GetAll'
        }
        $result=$null
        $result=Invoke-Api -Url $Url -Api $Api -Token $token
        if ( [bool]$result ) {
            # We have a result
            if ( $result -is [array] ) {
                # There a more than one
                Write-Verbose "  [MESG ] $($result.Count) results found"
            } else {
                # There can be only one
                Write-Verbose "  [MESG ] One result found"
            }
            $result| ForEach {
                Add-Member -InputObject $_ -NotePropertyMembers @{
                    'SourceURL' = $url; 'created' =[datetime]$($_.createdTimeStamp);
                    'uploaded'=[datetime]$($_.uploadedTimeStamp)
                } -TypeName 'MCRA.Datasource' -PassThru }
        } else {
            Write-Verbose "  [MESG ] No result found"
        }
    } # Einde PROCESS
    END {
        # Schrijft een melding op het scherm indien nodig (bij -Verbose)
        Write-CmdLetStopMessage -ScriptProperty $scriptproperty
    } # Einde END
} # Einde Function
