#
# Deze Module is in principe leeg, want alle geexporteerde functies staan in aparte bestanden.
#
# Enkele functies die in alle functies worden gebruikt.
Function Write-CmdLetStartMessage {
<#
.SYNOPSIS
Schrijft een melding op het scherm in standaardformaat bij de aanroep van een cmdlet.

.PARAMETER Version
Het versienummer dat wordt meegegeven.

.PARAMETER Invoke
Een InvocationInfo object waarvan de gegevens worden geprint.

.NOTES
    Naam:           Write-CmdLetStartMessage
    Auteur:         HH
    Bron:
    Wijzigingen:    24-08-2018  HH     1.0     Script geboren
                    26-09-2018  HH     1.1     Automatisch parent invokation uitzoeken toegevoegd.
#>
    [OutputType('MCRA.InvokeProperties')]
    [CmdletBinding()]
    param(
        [Parameter( Mandatory=$False,Position=1)]
        [string]$version,
        [Parameter(Mandatory=$False,Position=2)]
        [System.Management.Automation.InvocationInfo]$invoke
    )
    $callStack = Get-PSCallStack
    if ( -not $invoke ) {
        if ($callStack.Count -gt 1) {
            $invoke=$callStack[1].InvocationInfo
        } else {
            $invoke=[pscustomobject]@{ MyCommand =''; Pipelineposition=''; Line='' }
        }
    }
    # Hack to fix prefix stuff. Must be improved.
    $thiscommand=$invoke.MyCommand -replace '-MCRA','-'
    if ( $versie ) {
        Write-Verbose "[BEGIN] $thiscommand versie $versie (c) $((Get-Date).year) $ThisCompanyName uitgevoerd door $env:USERNAME"
    } else {
        Write-Verbose "[BEGIN] $thiscommand (c) $((Get-Date).year) $ThisCompanyName uitgevoerd door $env:USERNAME"
    }
    # We gaan eerst de parameters evalueren die niet in de pipeline gaan.
    ForEach ($key in $invoke.BoundParameters.keys) {
        $value=Get-Variable $Key -ErrorAction SilentlyContinue
        if ( $value ) {
            Write-Verbose "  [PARAM] $thiscommand $key : $($value.value)"
        }
    }
    # Tenslotte geven we als return waarde een object met gegevens die we later kunnen hergebruiken.
    [pscustomobject][ordered]@{
        Invoke = $invoke
        Timer = [Diagnostics.Stopwatch]::StartNew()
        Version = $version
        Name = $thiscommand
        PSTypeName = 'MCRA.InvokeProperties'
    }
}

Function Write-CmdLetStopMessage {
<#
.SYNOPSIS
Schrijft een melding op het scherm in standaardformaat bij het stoppen van een cmdlet.

.PARAMETER ScriptProperty
Een ScriptProperty object, dat uit het Write-CmdLetStartMessage commando komt.

.NOTES
    Naam:           Write-CmdLetStartMessage
    Auteur:         HH
    Bron:
    Wijzigingen:    24-08-2018  HH     1.0     Script geboren
#>
    [CmdletBinding()]
    param(
        [Parameter( Mandatory=$False,Position=1)]
        [object]$scriptproperty
    )

    if ( $scriptproperty ) {
        $scriptproperty.timer.Stop()
        Write-Verbose "[ END ] $($scriptproperty.invoke.MyCommand) ($($scriptproperty.timer.Elapsed.ToString('hh\:mm\:ss\.fff')))."
    } else {
        Write-Verbose "[ END ] Einde script."
    }
}

Function Get-LocalOSVersion {
<#
.SYNOPSIS
Vraagt lokale OS versie op.

.NOTES
    Naam:           Get-LocalOSVersion
    Auteur:         HH
    Bron:           HH
    Wijzigingen:    24-08-2018  HH     1.0     Script geboren
#>
    [OutputType('System.Version')]
    [CmdletBinding()]
    param()
    # Strict modus
    Set-StrictMode -Version Latest
    # Een versienummer van het script.
    Set-Variable versie -option Constant -value "1.0" -WhatIf:$false
    # Schrijf een melding op het scherm indien nodig (bij -Verbose)
    $scriptproperty=Write-CmdLetStartMessage -Invoke $MyInvocation -Version $versie

    [System.Version](Get-CimInstance Win32_OperatingSystem).version

    # Schrijft een melding op het scherm indien nodig (bij -Verbose)
    Write-CmdLetStopMessage -ScriptProperty $scriptproperty
}

Set-Variable ThisCompanyName -option Constant -value 'WUR'
# We exporteren hier alles expliciet, maar in de Manifest (psd bestand) filteren we alles er weer uit.
Export-ModuleMember -Variable *
Export-ModuleMember -Function *

