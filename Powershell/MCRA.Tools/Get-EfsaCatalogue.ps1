Function Get-EfsaCatalogue {
<#
.SYNOPSIS
Requests an EFSA catalogue file.

.DESCRIPTION
Requests an EFSA catalogue file.

.PARAMETER JSON
The JSON string to be used as a request.

.PARAMETER CREDENTIAL
A Credential object. Only the password will be used.

.PARAMETER TargetName
A TargetName from the Windows Credential Manager.

.PARAMETER URL
The URL to which the request is made.

.EXAMPLE
Get-EfsaCatalogue -JSON $json -Targetname EFSA
Uses the string in $json to requests a catalogue; credentials from
EFSA are used.


.INPUTS
System.String

.NOTES
    Naam:           Get-EfsaCatalogue
    Auteur:         Hansvandenheuvel
    Bron:
    Wijzigingen:    10-03-2020  HH     1.0     Script geboren.

    This is actually just the following command
    Invoke-RestMethod -Uri 'https://openapi.efsa.europa.eu/api/catalogues/catalogue-file' -Headers @{ 'Ocp-Apim-Subscription-Key' = 'secret' } -Method POST -ContentType 'application/json' -body $JsonString

#>

    #
    # (c) WUR
    # Pas het versienummer hieronder en hierboven aan als je iets wijzigt.
    # Geef bij .NOTES hierboven aan wat je precies hebt gewijzigd.
    #
    # Pas het Outputtype aan als je een object teruggeeft.
    [OutputType('My.Object')]
    [CmdletBinding(SupportsShouldProcess = $True)]
    Param(
        [Parameter(Mandatory=$True,ValueFromPipeline=$true,Position=1)]
        [string]$JSON,
        [Parameter(Mandatory=$true,ParameterSetName='Credential')]
        [System.Management.Automation.PSCredential]$Credential,
        [Parameter(Mandatory=$true,ParameterSetName='TargetName')]
        [string]$TargetName,
        [Parameter()]
        [string]$url='https://openapi.efsa.europa.eu',
        [Parameter()]
        [string]$Path,
        [Parameter()]
        [switch]$PassThru

    )
    BEGIN   {
        # Strict modus
        Set-StrictMode -Version Latest
        # Een versienummer van het script.
        Set-Variable versie -option Constant -value '1.0' -WhatIf:$false
        # Schrijf een melding op het scherm indien nodig (bij -Verbose)
        $scriptproperty=Write-CmdLetStartMessage -Invoke $MyInvocation -Version $versie

        if ($PSCmdlet.ParameterSetName -eq 'TargetName') {
            $Cred=Get-Credential -TargetName $TargetName
        } else {
            $Cred=$Credential
        }
        $JsonString = ''

    } # Einde BEGIN
    PROCESS {
        # Unfortunaly we have to swallow the entire JSON string, before processing.
        # No problem, it's usually small.
        $JsonString+=$JSON

    } # Einde PROCESS
    END {
        $result=New-Object xml
        $pUrl=$url+'/api/'+'catalogues/catalogue-file'
        $headers = @{ 'Ocp-Apim-Subscription-Key' = $Cred.GetNetworkCredential().Password }
        Write-Verbose "Invoking RestMethod to $pUrl"
        $dump=(Invoke-RestMethod -Uri $pUrl -Headers $headers -Method POST -ContentType 'application/json' -body $JsonString) -Split [Environment]::NewLine|ForEach -Begin {
            # Ugly hack, because no proper MIME handling is available in Powershell (that I am aware of)
            $xml = $False } -Process { if ($xml) {
                if ($_ -match '^\-\-uuid\:') {
                    $xml=$False
                } else {
                    $_
                }
            } else {
                if ($_ -match '^\<\?xml') {
                    $xml = $True
                    $_
                }
            }
        }
        $result=New-Object xml
        Write-Verbose "Converting to XML"
        $result.LoadXML($dump)
        # Write to file if requested, else dump object
        if ( $Path ) {
            if ($PassThru ) { $result }
            Write-Verbose "Saving to $Path"
            $result.Save($Path)
        } else {
            $result
        }
        $result
        # Schrijft een melding op het scherm indien nodig (bij -Verbose)
        Write-CmdLetStopMessage -ScriptProperty $scriptproperty
    } # Einde END
} # Einde Function
