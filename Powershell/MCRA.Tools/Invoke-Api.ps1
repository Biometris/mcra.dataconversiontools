Function Invoke-Api {
<#
.SYNOPSIS
Makes a call to the MCRA Api.

.DESCRIPTION
Makes a call to the MCRA Api.

.PARAMETER Url
The URL to which the request is made.

.PARAMETER Api
The Api call.

.PARAMETER Token
An object with a token

.EXAMPLE
Invoke-Api -Url 'https://mcra.test.wur.nl/Mcra90' -Api 'Repositories/GetAll' -token $token


.INPUTS
System.String

.NOTES
    Naam:           Invoke-Api
    Auteur:         Heuve081
    Bron:
    Wijzigingen:    11-03-2020  Heuve081     1.0     Script geboren.

#>

    #
    # (c) Private use
    # Pas het versienummer hieronder en hierboven aan als je iets wijzigt.
    # Geef bij .NOTES hierboven aan wat je precies hebt gewijzigd.
    #
    # Pas het Outputtype aan als je een object teruggeeft.
    [CmdletBinding(SupportsShouldProcess = $True)]
    Param(
        [Parameter(Mandatory=$true)]
        [string]$Url,
        $Token,
        [string]$Api
    )
    BEGIN   {
        # Strict modus
        Set-StrictMode -Version Latest
        # Een versienummer van het script.
        Set-Variable versie -option Constant -value '1.0' -WhatIf:$false
        # Schrijf een melding op het scherm indien nodig (bij -Verbose)
        $scriptproperty=Write-CmdLetStartMessage -Invoke $MyInvocation -Version $versie

        $ApiUrl = $Url+'/Api/'
    } # Einde BEGIN
    PROCESS {
        $RequestUrl=$ApiUrl+$Api
        $result=$null
        Write-Verbose "  [MESG ] Using request api URL $RequestUrl"
        Try {
            $result=Invoke-RestMethod -Uri $RequestUrl -Headers $token.header -ContentType 'application/json'
        } Catch {
            $result=$null
        }
        if ( -not [bool]$result ) {
            # We have no result
            Write-Verbose "  [MESG ] No result found"
        }
        $result
    } # Einde PROCESS
    END {
        # Schrijft een melding op het scherm indien nodig (bij -Verbose)
        Write-CmdLetStopMessage -ScriptProperty $scriptproperty
    } # Einde END
} # Einde Function
