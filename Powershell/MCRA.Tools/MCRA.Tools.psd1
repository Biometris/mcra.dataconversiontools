#
# Module manifest for module 'MCRA.Tools'
#
# Generated on: 10-03-2020
#

@{

# Script module or binary module file associated with this manifest.
RootModule = 'MCRA.Tools.psm1'

# Version number of this module.
ModuleVersion = '2.0.0.0'

# ID used to uniquely identify this module
GUID = '9af6f6e5-ef04-4322-b4ca-4fd59712f2dd'

# Author of this module
Author = 'Hans van den Heuvel'

# Company or vendor of this module
CompanyName = 'Wageningen University and Research, Biometris'

# Copyright statement for this module
Copyright = '(c) 2020. All rights reserved.'

# Description of the functionality provided by this module
# Description = 'Voorziet in standaardscripts voor de Missing Piece omgeving.'

# Minimum version of the Windows PowerShell engine required by this module
# PowerShellVersion = ''

# Name of the Windows PowerShell host required by this module
# PowerShellHostName = ''

# Minimum version of the Windows PowerShell host required by this module
# PowerShellHostVersion = ''

# Minimum version of Microsoft .NET Framework required by this module
# DotNetFrameworkVersion = ''

# Minimum version of the common language runtime (CLR) required by this module
# CLRVersion = ''

# Processor architecture (None, X86, Amd64) required by this module
# ProcessorArchitecture = ''

# Modules that must be imported into the global environment prior to importing this module
# RequiredModules = @()

# Assemblies that must be loaded prior to importing this module
# RequiredAssemblies = @()

# Script files (.ps1) that are run in the caller's environment prior to importing this module.
# ScriptsToProcess = @('Set-Variables.ps1')

# Type files (.ps1xml) to be loaded when importing this module
# TypesToProcess = @( )

# Format files (.ps1xml) to be loaded when importing this module
FormatsToProcess = @(   'Credential.ps1xml',
                        'MCRA.Repository.ps1xml',
                        'MCRA.DataSource.ps1xml',
                        'MCRA.Workspace.ps1xml')

# Modules to import as nested modules of the module specified in RootModule/ModuleToProcess
NestedModules = @(  'Command-Credential.ps1',
                    'Get-EfsaCatalogue.ps1',
                    'Get-Repository.ps1',
                    'Get-DataSource.ps1',
                    'Get-File.ps1',
                    'Get-Workspace.ps1',
                    'Get-Token.ps1',
                    'Invoke-Api.ps1'
                    )

# Functions to export from this module
FunctionsToExport = @(  'Clear-Credential','Get-Credential','Read-Credential','Write-Credential',
                        'Get-EfsaCatalogue',
                        'Get-Repository',
                        'Get-DataSource',
                        'Get-File',
                        'Get-Workspace'
                        )

# Cmdlets to export from this module
CmdletsToExport = '*'

# Variables to export from this module
# VariablesToExport = @()

# Aliases to export from this module
AliasesToExport = '*'

# List of all modules packaged with this module
# ModuleList = @()

# List of all files packaged with this module
# FileList = @()

# Private data to pass to the module specified in RootModule/ModuleToProcess
# PrivateData = ''
# PrivateData = @{ Token2 = 'global'}

# HelpInfo URI of this module
# HelpInfoURI = ''

# Default prefix for commands exported from this module. Override the default prefix using Import-Module -Prefix.
DefaultCommandPrefix = 'MCRA'

}
