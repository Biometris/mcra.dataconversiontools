#
#
# Aangepast door HH, om meer in lijn met de standaard Piece naamgeving te komen.
# dd 06-11-2016
# Enum-Creds        -> Read-Credential (gecombineerd)
# Read-Creds        -> Read-Credential
# Del-Creds         -> Clear-Credential
# Write-Creds       -> Write-Credential
# Read-Creds        -> Get-Credential (geeft PSCredential object)
# Write-Creds       -> Set-Credential (gebruikt PSCredential object)
#

# Author: Jim Harrison (jim@isatools.org)
# Date  : 2012/05/20
# Vers  : 1.5

# * I STRONGLY recommend that you become familiar
# * with http://msdn.microsoft.com/en-us/library/windows/desktop/aa374788(v=vs.85).aspx
# * before you create new credentials with -CredType other than "GENERIC"

# http://msdn.microsoft.com/en-us/library/windows/desktop/aa374788(v=vs.85).aspx
# http://stackoverflow.com/questions/7162604/get-cached-credentials-in-powershell-from-windows-7-credential-manager
# http://msdn.microsoft.com/en-us/library/windows/desktop/aa374788(v=vs.85).aspx
# http://blogs.msdn.com/b/peerchan/archive/2005/11/01/487834.aspx


# De C-code om toegang te krijgen.
[String] $PsCredmanUtils = @"
using System;
using System.Runtime.InteropServices;

namespace PsUtils
{
    public class CredMan
    {
        #region Imports
        // DllImport derives from System.Runtime.InteropServices
        [DllImport("Advapi32.dll", SetLastError = true, EntryPoint = "CredDeleteW", CharSet = CharSet.Unicode)]
        private static extern bool CredDeleteW([In] string target, [In] CRED_TYPE type, [In] int reservedFlag);

        [DllImport("Advapi32.dll", SetLastError = true, EntryPoint = "CredEnumerateW", CharSet = CharSet.Unicode)]
        private static extern bool CredEnumerateW([In] string Filter, [In] int Flags, out int Count, out IntPtr CredentialPtr);

        [DllImport("Advapi32.dll", SetLastError = true, EntryPoint = "CredFree")]
        private static extern void CredFree([In] IntPtr cred);

        [DllImport("Advapi32.dll", SetLastError = true, EntryPoint = "CredReadW", CharSet = CharSet.Unicode)]
        private static extern bool CredReadW([In] string target, [In] CRED_TYPE type, [In] int reservedFlag, out IntPtr CredentialPtr);

        [DllImport("Advapi32.dll", SetLastError = true, EntryPoint = "CredWriteW", CharSet = CharSet.Unicode)]
        private static extern bool CredWriteW([In] ref Credential userCredential, [In] UInt32 flags);
        #endregion

        #region Fields
        public enum CRED_FLAGS : uint
        {
            NONE = 0x0,
            PROMPT_NOW = 0x2,
            USERNAME_TARGET = 0x4
        }

        public enum CRED_ERRORS : uint
        {
            ERROR_SUCCESS = 0x0,
            ERROR_INVALID_PARAMETER = 0x80070057,
            ERROR_INVALID_FLAGS = 0x800703EC,
            ERROR_NOT_FOUND = 0x80070490,
            ERROR_NO_SUCH_LOGON_SESSION = 0x80070520,
            ERROR_BAD_USERNAME = 0x8007089A
        }

        public enum CRED_PERSIST : uint
        {
            SESSION = 1,
            LOCAL_MACHINE = 2,
            ENTERPRISE = 3
        }

        public enum CRED_TYPE : uint
        {
            GENERIC = 1,
            DOMAIN_PASSWORD = 2,
            DOMAIN_CERTIFICATE = 3,
            DOMAIN_VISIBLE_PASSWORD = 4,
            GENERIC_CERTIFICATE = 5,
            DOMAIN_EXTENDED = 6,
            MAXIMUM = 7,      // Maximum supported cred type
            MAXIMUM_EX = (MAXIMUM + 1000),  // Allow new applications to run on old OSes
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public struct Credential
        {
            public CRED_FLAGS Flags;
            public CRED_TYPE Type;
            public string TargetName;
            public string Comment;
            public DateTime LastWritten;
            public UInt32 CredentialBlobSize;
            public string CredentialBlob;
            public CRED_PERSIST Persist;
            public UInt32 AttributeCount;
            public IntPtr Attributes;
            public string TargetAlias;
            public string UserName;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        private struct NativeCredential
        {
            public CRED_FLAGS Flags;
            public CRED_TYPE Type;
            public IntPtr TargetName;
            public IntPtr Comment;
            public System.Runtime.InteropServices.ComTypes.FILETIME LastWritten;
            public UInt32 CredentialBlobSize;
            public IntPtr CredentialBlob;
            public UInt32 Persist;
            public UInt32 AttributeCount;
            public IntPtr Attributes;
            public IntPtr TargetAlias;
            public IntPtr UserName;
        }
        #endregion

        #region Child Class
        private class CriticalCredentialHandle : Microsoft.Win32.SafeHandles.CriticalHandleZeroOrMinusOneIsInvalid
        {
            public CriticalCredentialHandle(IntPtr preexistingHandle)
            {
                SetHandle(preexistingHandle);
            }

            private Credential XlateNativeCred(IntPtr pCred)
            {
                NativeCredential ncred = (NativeCredential)Marshal.PtrToStructure(pCred, typeof(NativeCredential));
                Credential cred = new Credential();
                cred.Type = ncred.Type;
                cred.Flags = ncred.Flags;
                cred.Persist = (CRED_PERSIST)ncred.Persist;

                long LastWritten = ncred.LastWritten.dwHighDateTime;
                LastWritten = (LastWritten << 32) + ncred.LastWritten.dwLowDateTime;
                cred.LastWritten = DateTime.FromFileTime(LastWritten);

                cred.UserName = Marshal.PtrToStringUni(ncred.UserName);
                cred.TargetName = Marshal.PtrToStringUni(ncred.TargetName);
                cred.TargetAlias = Marshal.PtrToStringUni(ncred.TargetAlias);
                cred.Comment = Marshal.PtrToStringUni(ncred.Comment);
                cred.CredentialBlobSize = ncred.CredentialBlobSize;
                if (0 < ncred.CredentialBlobSize)
                {
                    cred.CredentialBlob = Marshal.PtrToStringUni(ncred.CredentialBlob, (int)ncred.CredentialBlobSize / 2);
                }
                return cred;
            }

            public Credential GetCredential()
            {
                if (IsInvalid)
                {
                    throw new InvalidOperationException("Invalid CriticalHandle!");
                }
                Credential cred = XlateNativeCred(handle);
                return cred;
            }

            public Credential[] GetCredentials(int count)
            {
                if (IsInvalid)
                {
                    throw new InvalidOperationException("Invalid CriticalHandle!");
                }
                Credential[] Credentials = new Credential[count];
                IntPtr pTemp = IntPtr.Zero;
                for (int inx = 0; inx < count; inx++)
                {
                    pTemp = Marshal.ReadIntPtr(handle, inx * IntPtr.Size);
                    Credential cred = XlateNativeCred(pTemp);
                    Credentials[inx] = cred;
                }
                return Credentials;
            }

            override protected bool ReleaseHandle()
            {
                if (IsInvalid)
                {
                    return false;
                }
                CredFree(handle);
                SetHandleAsInvalid();
                return true;
            }
        }
        #endregion

        #region Custom API
        public static int CredDelete(string target, CRED_TYPE type)
        {
            if (!CredDeleteW(target, type, 0))
            {
                return Marshal.GetHRForLastWin32Error();
            }
            return 0;
        }

        public static int CredEnum(string Filter, out Credential[] Credentials)
        {
            int count = 0;
            int Flags = 0x0;
            if (string.IsNullOrEmpty(Filter) ||
                "*" == Filter)
            {
                Filter = null;
                if (6 <= Environment.OSVersion.Version.Major)
                {
                    Flags = 0x1; //CRED_ENUMERATE_ALL_CREDENTIALS; only valid is OS >= Vista
                }
            }
            IntPtr pCredentials = IntPtr.Zero;
            if (!CredEnumerateW(Filter, Flags, out count, out pCredentials))
            {
                Credentials = null;
                return Marshal.GetHRForLastWin32Error();
            }
            CriticalCredentialHandle CredHandle = new CriticalCredentialHandle(pCredentials);
            Credentials = CredHandle.GetCredentials(count);
            return 0;
        }

        public static int CredRead(string target, CRED_TYPE type, out Credential Credential)
        {
            IntPtr pCredential = IntPtr.Zero;
            Credential = new Credential();
            if (!CredReadW(target, type, 0, out pCredential))
            {
                return Marshal.GetHRForLastWin32Error();
            }
            CriticalCredentialHandle CredHandle = new CriticalCredentialHandle(pCredential);
            Credential = CredHandle.GetCredential();
            return 0;
        }

        public static int CredWrite(Credential userCredential)
        {
            if (!CredWriteW(ref userCredential, 0))
            {
                return Marshal.GetHRForLastWin32Error();
            }
            return 0;
        }

        #endregion

        private static int AddCred()
        {
            Credential Cred = new Credential();
            string Password = "Password";
            Cred.Flags = 0;
            Cred.Type = CRED_TYPE.GENERIC;
            Cred.TargetName = "Target";
            Cred.UserName = "UserName";
            Cred.AttributeCount = 0;
            Cred.Persist = CRED_PERSIST.ENTERPRISE;
            Cred.CredentialBlobSize = (uint)Password.Length;
            Cred.CredentialBlob = Password;
            Cred.Comment = "Comment";
            return CredWrite(Cred);
        }

        private static bool CheckError(string TestName, CRED_ERRORS Rtn)
        {
            switch(Rtn)
            {
                case CRED_ERRORS.ERROR_SUCCESS:
                    Console.WriteLine(string.Format("'{0}' worked", TestName));
                    return true;
                case CRED_ERRORS.ERROR_INVALID_FLAGS:
                case CRED_ERRORS.ERROR_INVALID_PARAMETER:
                case CRED_ERRORS.ERROR_NO_SUCH_LOGON_SESSION:
                case CRED_ERRORS.ERROR_NOT_FOUND:
                case CRED_ERRORS.ERROR_BAD_USERNAME:
                    Console.WriteLine(string.Format("'{0}' failed; {1}.", TestName, Rtn));
                    break;
                default:
                    Console.WriteLine(string.Format("'{0}' failed; 0x{1}.", TestName, Rtn.ToString("X")));
                    break;
            }
            return false;
        }

        /*
         * Note: the Main() function is primarily for debugging and testing in a Visual
         * Studio session.  Although it will work from PowerShell, it's not very useful.
         */
        public static void Main()
        {
            Credential[] Creds = null;
            Credential Cred = new Credential();
            int Rtn = 0;

            Console.WriteLine("Testing CredWrite()");
            Rtn = AddCred();
            if (!CheckError("CredWrite", (CRED_ERRORS)Rtn))
            {
                return;
            }
            Console.WriteLine("Testing CredEnum()");
            Rtn = CredEnum(null, out Creds);
            if (!CheckError("CredEnum", (CRED_ERRORS)Rtn))
            {
                return;
            }
            Console.WriteLine("Testing CredRead()");
            Rtn = CredRead("Target", CRED_TYPE.GENERIC, out Cred);
            if (!CheckError("CredRead", (CRED_ERRORS)Rtn))
            {
                return;
            }
            Console.WriteLine("Testing CredDelete()");
            Rtn = CredDelete("Target", CRED_TYPE.GENERIC);
            if (!CheckError("CredDelete", (CRED_ERRORS)Rtn))
            {
                return;
            }
            Console.WriteLine("Testing CredRead() again");
            Rtn = CredRead("Target", CRED_TYPE.GENERIC, out Cred);
            if (!CheckError("CredRead", (CRED_ERRORS)Rtn))
            {
                Console.WriteLine("if the error is 'ERROR_NOT_FOUND', this result is OK.");
            }
        }
    }
}
"@

$PsCredMan = $null
try
{
    $PsCredMan = [PsUtils.CredMan]
}
catch
{
    #only remove the error we generate
    if ( $Error.Count -gt 1 ){
        $Error.RemoveAt($Error.Count-1)
    }
}
if($null -eq $PsCredMan)
{
    Add-Type $PsCredmanUtils
}

#region Internal Tools
[HashTable] $ErrorCategory = @{0x80070057 = "InvalidArgument";
                               0x800703EC = "InvalidData";
                               0x80070490 = "ObjectNotFound";
                               0x80070520 = "SecurityError";
                               0x8007089A = "SecurityError"}

# Interne functies:
function Get-CredType
{
    Param
    (
        [Parameter(Mandatory=$true)][ValidateSet( "GENERIC",
                                                  "DOMAIN_PASSWORD",
                                                  "DOMAIN_CERTIFICATE",
                                                  "DOMAIN_VISIBLE_PASSWORD",
                                                  "GENERIC_CERTIFICATE",
                                                  "DOMAIN_EXTENDED",
                                                  "MAXIMUM",
                                                  "MAXIMUM_EX")][String] $CredType
    )

    switch($CredType)
    {
        "GENERIC" {return [PsUtils.CredMan+CRED_TYPE]::GENERIC}
        "DOMAIN_PASSWORD" {return [PsUtils.CredMan+CRED_TYPE]::DOMAIN_PASSWORD}
        "DOMAIN_CERTIFICATE" {return [PsUtils.CredMan+CRED_TYPE]::DOMAIN_CERTIFICATE}
        "DOMAIN_VISIBLE_PASSWORD" {return [PsUtils.CredMan+CRED_TYPE]::DOMAIN_VISIBLE_PASSWORD}
        "GENERIC_CERTIFICATE" {return [PsUtils.CredMan+CRED_TYPE]::GENERIC_CERTIFICATE}
        "DOMAIN_EXTENDED" {return [PsUtils.CredMan+CRED_TYPE]::DOMAIN_EXTENDED}
        "MAXIMUM" {return [PsUtils.CredMan+CRED_TYPE]::MAXIMUM}
        "MAXIMUM_EX" {return [PsUtils.CredMan+CRED_TYPE]::MAXIMUM_EX}
    }
}

function Get-CredPersist
{
    Param
    (
        [Parameter(Mandatory=$true)][ValidateSet("SESSION",
                                                  "LOCAL_MACHINE",
                                                  "ENTERPRISE")][String] $CredPersist
    )

    switch($CredPersist)
    {
        "SESSION" {return [PsUtils.CredMan+CRED_PERSIST]::SESSION}
        "LOCAL_MACHINE" {return [PsUtils.CredMan+CRED_PERSIST]::LOCAL_MACHINE}
        "ENTERPRISE" {return [PsUtils.CredMan+CRED_PERSIST]::ENTERPRISE}
    }
}

# Functies die geexporteerd worden:

Function Clear-Credential {
<#
.SYNOPSIS
Verwijdert credentials uit de Windows Credential store.

.DESCRIPTION
Verwijdert credentials uit de Windows Credential store, d.m.v. een calls
naar Win32 CredDeleteW via [PsUtils.CredMan]::CredDelete.

.PARAMETER Target
Het doelnaam van de credential, in de credentialstore.

.PARAMETER Type
Het type credential; standaard "GENERIC". Mogelijke waarden zijn:
GENERIC, DOMAIN_PASSWORD, DOMAIN_CERTIFICATE, DOMAIN_VISIBLE_PASSWORD,
GENERIC_CERTIFICATE, DOMAIN_EXTENDED, MAXIMUM, MAXIMUM_EX.

.PARAMETER Credential
Via de pipeline kun je ook een CredMan+Credential object doorgeven,
zodat je dit commando 'achter' bijvoorbeeld Read-Credential kunt zetten.

.EXAMPLE
Clear-Credential -Target 'iets'
Verwijdert de credentials met targetname 'iets' en Type 'GENERIC'.

.EXAMPLE
Read-Credential | Clear-Credential
Verwijdert alle Windows credentials.

.INPUTS
[PsUtils.CredMan+Credential]

.OUTPUTS
[Management.Automation.ErrorRecord] by een fout.

.NOTES
    Naam:           Clear-Credential
    Auteur:         Jim Harrison (jim@isatools.org)
    Bron:           https://gallery.technet.microsoft.com/scriptcenter/PowerShell-Credentials-d44c3cde
    Wijzigingen:    20-05-2012  Jim Harrison    1.5     Van Internet
                    14-09-2016  HH         1.6     Aanpassingen om er een module van te maken;
                                                        Namen van de functies aangepast, meer in lijn
                                                        met Powershell.
                    10-11-2016  HH         1.7     Verdere aanpassingen aan naamgeving en output objecten
                                                        Input via pipeline, teksten vertaald, twee parametersets
                                                        gemaakt.
                    24-08-2017  HH         1.8     TargetName als positionele parameter aangemaakt.
                    24-08-2018  HH         1.9     Aangepast aan nieuwe scriptsjabloon

#>

    [CmdletBinding(
        SupportsShouldProcess = $True,
        DefaultParameterSetName='SeparateFields'
    )]
    Param(
        [Parameter(Mandatory=$true,ParameterSetName='SeparateFields',Position=1)]
        [ValidateLength(1,32767)]
        [String] $TargetName,
        [Parameter(Mandatory=$false,ParameterSetName='SeparateFields')]
        [ValidateSet("GENERIC",
                      "DOMAIN_PASSWORD",
                      "DOMAIN_CERTIFICATE",
                      "DOMAIN_VISIBLE_PASSWORD",
                      "GENERIC_CERTIFICATE",
                      "DOMAIN_EXTENDED",
                      "MAXIMUM",
                      "MAXIMUM_EX")]
        [String] $Type = "GENERIC",
        [Parameter(ValueFromPipeline=$True,ParameterSetName='FieldsInObject')]
        [PsUtils.CredMan+Credential[]] $Credential
    )
    BEGIN   {
        # Strict modus
        Set-StrictMode -Version Latest
        # Een versienummer van het script.
        Set-Variable versie -option Constant -value "1.9" -WhatIf:$false
        # Schrijf een melding op het scherm indien nodig (bij -Verbose)
        $scriptproperty=Write-CmdLetStartMessage -Version $versie

    } # Einde BEGIN
    PROCESS {
        if ( $PSCmdlet.ParameterSetName -eq 'SeparateFields' ) {
            # Er worden losse parameters doorgegeven. Even een array-object van maken.
            [PsUtils.CredMan+Credential] $Credential = New-Object PsUtils.CredMan+Credential
            $Credential.TargetName=$TargetName
            $Credential.Type=$Type
        }
        ForEach ( $cred in $Credential ) {
            [Int] $Results = 0
            if ($pscmdlet.ShouldProcess($cred.TargetName, "Credentials zouden worden verwijderd.")) {
                try {
                    $Results = [PsUtils.CredMan]::CredDelete($cred.TargetName, $(Get-CredType $cred.Type))
                }
                catch {
                    Write-Warning "  [WARN ] $($MyInvocation.MyCommand) Fout bij het verwijderen van credentials $($cred.TargetName), namelijk $($_.Exception.Message)."
                }
            }
        }
    } # Einde PROCESS
    END     {
        # Schrijft een melding op het scherm indien nodig (bij -Verbose)
        Write-CmdLetStopMessage -ScriptProperty $scriptproperty
    } # Einde END
}

Function Read-Credential {
<#
.SYNOPSIS
Haalt de credentials op uit de Windows Credential store.

.DESCRIPTION
Haalt de credentials op uit de Credential store, via een calls naar Win32
CredEnumerateW via [PsUtils.CredMan]::CredEnum.

.PARAMETER TargetName
De naam van de credential.

.EXAMPLE
Read-Credential -TargetName 'EFSA'
Haalt de credential gegevens op van 'EFSA'.

.EXAMPLE
Read-Credential | Clear-Credential
Verwijdert alle Windows credentials.

.EXAMPLE
Read-Credential|Format-Table -View password
Geeft de wachtwoorden in platte tekst weer, voor zover mogelijk.

.EXAMPLE
Read-Credential|Select *
Geeft alle velden weer, dus ook de wachtwoorden.

.EXAMPLE
(Read-Credential).CredentialBlob
Geeft alle wachtwoorden weer, zoals die zijn opgeslagen.

.EXAMPLE
[Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR(((read-credential 'EFSA').CredentialBlob|ConvertTo-SecureString)))
Als het wachtwoord als secure is opgeslagen, wordt het vertaald naar platte tekst.

.INPUTS

.OUTPUTS
[PsUtils.CredMan+Credential[]] na succes.
[Management.Automation.ErrorRecord] bij fout.

.NOTES
    Naam:           Read-Credential
    Auteur:         Jim Harrison (jim@isatools.org)
    Bron:           https://gallery.technet.microsoft.com/scriptcenter/PowerShell-Credentials-d44c3cde
    Wijzigingen:    20-05-2012  Jim Harrison    1.5     Van Internet
                    14-09-2016  HH                 Aanpassingen om er een module van te maken;
                                                        Namen van de functies aangepast, meer in lijn
                                                        met Powershell.
                    10-11-2016  HH         1.7     Verdere aanpassingen zodat het script volledig in lijn
                                                        is met scripts. Help in nederlands vertaald.
                    24-08-2017  HH         1.8     Parameter positional gemaakt, zodat default de TargetName kan worden gebruikt.
                    24-08-2018  HH         1.9     Aangepast aan nieuwe scriptsjabloon

#>

#
# (c) WUR
# Pas het versienummer hieronder en hierboven aan als je iets wijzigt.
# Geef bij .NOTES hierboven aan wat je precies hebt gewijzigd.
#
    [OutputType('PsUtils.CredMan+Credential')]
    [CmdletBinding(
        SupportsShouldProcess = $True # Hiermee zorg je dat WhatIf en Confirm gebruikt kunnen worden :-)
    )]
    Param(
        [Parameter(Mandatory=$false,Position=1)]
        [AllowEmptyString()]
        [String] $TargetName = [String]::Empty
    )
    BEGIN   {
        # Strict modus
        Set-StrictMode -Version Latest
        # Een versienummer van het script.
        Set-Variable versie -option Constant -value "1.9" -WhatIf:$false
        # Schrijf een melding op het scherm indien nodig (bij -Verbose)
        $scriptproperty=Write-CmdLetStartMessage -Version $versie

    } # Einde BEGIN
    PROCESS {
        [PsUtils.CredMan+Credential[]] $Creds = [Array]::CreateInstance([PsUtils.CredMan+Credential], 0)
        [Int] $Results = 0
        try {
            $Results = [PsUtils.CredMan]::CredEnum($TargetName, [Ref]$Creds)
        }
        catch {
            return $_
        }
        switch($Results) {
            0           { break }
            0x80070490  { break } #ERROR_NOT_FOUND
            default {
                Write-Warning "  [WARN ] $($MyInvocation.MyCommand) Fout bij het lezen van credentials $($cred.TargetName), namelijk $($_.Exception.Message)."
            }
        }
        $Creds
    } # Einde PROCESS
    END     {
        # Schrijft een melding op het scherm indien nodig (bij -Verbose)
        Write-CmdLetStopMessage -ScriptProperty $scriptproperty
    } # Einde END
} # Einde Function

Function Write-Credential {
<#
.SYNOPSIS
Schrijft gegevens in de Windows Credential store.

.DESCRIPTION
Schrijft gegevens weg in de Credential store. Met dit commando kun je nieuwe gegevens wegschrijven
of bestaande gegevens bijwerken. Dit gebeurt d.m.v. een call naar Win32 CredWriteW via
[PsUtils.CredMan]::CredWrite.

.INPUTS

.OUTPUTS

.PARAMETER TargetName
De naam waaronder de credential wordt weggeschreven in de store.

.PARAMETER UserName
De gebruikersnaam van het weg te schrijven credential.

.PARAMETER Password
Het wachtwoord van het weg te schrijven credential.

.PARAMETER Secure
Als je de parameters Username/Password gebruikt, wordt het wachtwoord standaard als platte tekst
opgeslagen in de credential store, zoals met veel wachtwoorden gebeurt.
Mocht je liever een Powershell securestring opslaan (=.Net SecureString Class) dan moet
je deze parameter opgeven.

.PARAMETER Credential
De gebruikersnaam en het wachtwoord in een PSCredential object. Het wachtwoord wordt als Securestring
opgeslagen. Je moet kiezen tussen het gebruik van de parameters UserName/Password/Secure enerzijds,
of Credential anderzijds.

.PARAMETER Comment
Eventueel commentaar dat je bij de credential wilt noteren.

.PARAMETER Type
Het type credential; standaard "GENERIC". Mogelijke waarden zijn:
GENERIC, DOMAIN_PASSWORD, DOMAIN_CERTIFICATE, DOMAIN_VISIBLE_PASSWORD,
GENERIC_CERTIFICATE, DOMAIN_EXTENDED, MAXIMUM, MAXIMUM_EX. Er wordt sterk
geadviseerd alleen GENERIC te gebruiken (de standaardwaarde)

.PARAMETER Persist
De persistance van de credential. Standaard is dit "ENTERPRISE". Mogelijke
waardes: SESSION, LOCAL_MACHINE, ENTERPRISE.

.EXAMPLE
Write-Credential -Target 'Test' -Credential $creds
Slaat de credentials uit het $creds object op in de credential store.

.EXAMPLE
Write-Credential -Target 'Testuser' -Credential (Get-Credential)
Geeft een grafische dialoog, en slaat de username/password op onder 'Testuser' als SecureString.

.EXAMPLE
Write-Credential -Target 'Test' -UserName 'Joop' -Password 'WatEenSlechtWachtwoordIsDitToch'
Slaat de credentials op met gebruikersnaam 'Joop' en wachtwoord 'WatEenSlechtWachtwoordIsDitToch',
als leesbare tekst.

.EXAMPLE
Write-Credential
Doorloopt een tekstdialoog waarin de naam van de credential, de username en password wordt gevraagd.

.NOTES
    Naam:           Write-Credential
    Auteur:         Jim Harrison (jim@isatools.org)
    Bron:           https://gallery.technet.microsoft.com/scriptcenter/PowerShell-Credentials-d44c3cde
    Wijzigingen:    20-05-2012  Jim Harrison    1.5     Van Internet
                    14-09-2016  HH                 Aanpassingen om er een module van te maken;
                                                        Namen van de functies aangepast, meer in lijn
                                                        met Powershell.
                    10-11-2015  HH         1.7     Verdere aanpassingen om het volledig in lijn te
                                                        brengen met de standaard. Helptekst
                                                        aangepast. Teksten vertaald. Mogelijkheid tot direct
                                                        opgeven van PSCredential object als input.
                    24-08-2017  HH         1.8     Parameter TargetName positional gemaakt, zodat default
                                                        de TargetName kan worden gebruikt. Parameter Password
                                                        kan nu als gewone string worden opgegeven op de commandoregel,
                                                        of wordt anders als securestring via een tekstdialoog gevraagd.
                                                        Parameter Secure werkt nu ook!
                    24-08-2018  HH         1.9     Aangepast aan nieuwe scriptsjabloon

.LINK
    Zie ook:
        https://msdn.microsoft.com/en-us/library/system.security.securestring(v=vs.110).aspx)
        http://msdn.microsoft.com/en-us/library/windows/desktop/aa374788(v=vs.85).aspx
        https://gallery.technet.microsoft.com/scriptcenter/PowerShell-Credentials-d44c3cde

#>


#
# (c) WUR
# Pas het versienummer hieronder en hierboven aan als je iets wijzigt.
# Geef bij .NOTES hierboven aan wat je precies hebt gewijzigd.
#

    [CmdletBinding(
        SupportsShouldProcess = $True,
        DefaultParameterSetName='UserNamePassword'
    )]
    Param(
        [Parameter(Mandatory=$true,Position=1)]
        [ValidateLength(0,32676)]
        [String] $TargetName,
        [Parameter(Mandatory=$true,ParameterSetName='Credential')]
        [System.Management.Automation.PSCredential] $Credential,
        [Parameter(Mandatory=$true,ParameterSetName='UserNamePassword')]
        [ValidateLength(1,512)]
        [String]$UserName,
        [Parameter(ParameterSetName='UserNamePassword')]
        [ValidateLength(0,32676)]
        [string]$Password=$( if ( $PSCmdlet.ParameterSetName -eq 'UserNamePassword' ) { [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR((Read-Host "Password"  -AsSecureString))) } ),
        [Parameter(Mandatory=$false,ParameterSetName='UserNamePassword')]
        [Switch] $Secure = $false,
        [Parameter(Mandatory=$false)]
        [ValidateLength(0,256)]
        [String] $Comment = [String]::Format("Last edited by {0}\{1} on {2}",$Env:UserDomain,$Env:UserName,$Env:ComputerName),
        [Parameter(Mandatory=$false)]
        [ValidateSet(   "GENERIC",
                        "DOMAIN_PASSWORD",
                        "DOMAIN_CERTIFICATE",
                        "DOMAIN_VISIBLE_PASSWORD",
                        "GENERIC_CERTIFICATE",
                        "DOMAIN_EXTENDED",
                        "MAXIMUM",
                        "MAXIMUM_EX")]
        [String] $Type = "GENERIC",
        [Parameter(Mandatory=$false)]
        [ValidateSet(   "SESSION",
                        "LOCAL_MACHINE",
                        "ENTERPRISE")]
        [String] $Persist = "ENTERPRISE"
    )

    BEGIN   {
        # Strict modus
        Set-StrictMode -Version Latest
        # Een versienummer van het script.
        Set-Variable versie -option Constant -value "1.9" -WhatIf:$false
        # Schrijf een melding op het scherm indien nodig (bij -Verbose)
        $scriptproperty=Write-CmdLetStartMessage -Version $versie

        if("GENERIC" -ne $Type -and 337 -lt $TargetName.Length) { #CRED_MAX_DOMAIN_TARGET_NAME_LENGTH
            [String] $Msg = "Het Target veld is $($Target.Length) karakters lang, terwijl er maximaal 337 karakters zijn toegestaan."
            [Management.ManagementException] $MgmtException = New-Object Management.ManagementException($Msg)
            [Management.Automation.ErrorRecord] $ErrRcd = New-Object Management.Automation.ErrorRecord($MgmtException, 666, 'LimitsExceeded', $null)
            return $ErrRcd
        }

        if ( $PSCmdlet.ParameterSetName -eq 'Credential' ) {
            if ( $Secure ) {
                $Password=ConvertFrom-SecureString -SecureString $Credential.Password
            } else {
                $Password=$Credential.GetNetworkCredential().Password
            }
        } else {
            if ( $Secure ) {
                $Password=ConvertFrom-SecureString -SecureString (ConvertTo-SecureString $Password -AsPlainText -Force)
            }
        }

    } # Einde BEGIN
    PROCESS {
        [PsUtils.CredMan+Credential] $Cred = New-Object PsUtils.CredMan+Credential
        switch($TargetName -eq $UserName -and
               ("CRED_TYPE_DOMAIN_PASSWORD" -eq $Type -or
                "CRED_TYPE_DOMAIN_CERTIFICATE" -eq $Type))
        {
            $true  {$Cred.Flags = [PsUtils.CredMan+CRED_FLAGS]::USERNAME_TARGET}
            $false  {$Cred.Flags = [PsUtils.CredMan+CRED_FLAGS]::NONE}
        }
        $Cred.Type = Get-CredType $Type
        $Cred.TargetName = $TargetName
        $Cred.UserName = $UserName
        $Cred.AttributeCount = 0
        $Cred.Persist = Get-CredPersist $Persist
        $Cred.CredentialBlobSize = [Text.Encoding]::Unicode.GetBytes($Password).Length
        $Cred.CredentialBlob = $Password
        $Cred.Comment = $Comment

        [Int] $Results = 0
        if ($pscmdlet.ShouldProcess($cred.TargetName, "Credentials worden bijgewerkt.")) {
            try {
                $Results = [PsUtils.CredMan]::CredWrite($Cred)
            }
            catch {
                return $_
            }
        }

        if(0 -ne $Results) {
            [String] $Msg = "Fout bij het wegschrijven van de credentials naar de credential store voor '$Target' met gebruikersnaam '$UserName'"
            [Management.ManagementException] $MgmtException = New-Object Management.ManagementException($Msg)
            [Management.Automation.ErrorRecord] $ErrRcd = New-Object Management.Automation.ErrorRecord($MgmtException, $Results.ToString("X"), $ErrorCategory[$Results], $null)
            return $ErrRcd
        }
        #return $Results
    } # Einde PROCESS
    END {
        # Schrijft een melding op het scherm indien nodig (bij -Verbose)
        Write-CmdLetStopMessage -ScriptProperty $scriptproperty
    }
} # Einde Function

Function Get-Credential {
<#
.SYNOPSIS
Haalt PSCredential object uit de Windows Credential store.

.DESCRIPTION
Haalt de credentials op uit de Credential store, via Read-Credential en geeft een
PSCredential object terug.

.PARAMETER TargetName
De naam van de credential.

.EXAMPLE
Get-Credential -TargetName 'EFSA'
Haalt de credential gegevens op van 'EFSA'.

.EXAMPLE
(Get-Credential 'EFSA').GetNetworkCredential().Password
Haalt een PSCredential object op, en trekt het wachtwoord eruit in platte tekst.

.EXAMPLE
[Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR((Get-Credential 'EFSA').Password))
Haalt uit het PSCredential een SecureString, en trekt daar het wachtwoord uit in platte tekst.

.INPUTS

.OUTPUTS
[System.Management.Automation.PSCredential]

.NOTES
    Naam:           Get-Credential
    Auteur:         HH
    Bron:
    Wijzigingen:    10-11-2016  HH         1.0     Initiele versie.
                    13-11-2016  HH         1.1     Waarschuwing ipv foutmelding bij opvragen
                                                        van credential met leeg wachtwoord.
                    24-08-2017  HH         1.2     Parameter positional gemaakt, zodat default de TargetName kan worden gebruikt.
                    23-10-2017  HH         1.3     Bug eruit gehaald met de Try/Catch. Nu wordt beter gedetecteerd of de CredentialBlob
                                                        wel of geen securestring is.
                    24-08-2018  HH         1.4     Aangepast aan nieuwe scriptsjabloon
                    14-12-2019  HH              1.5     Waarschuwing optioneel gemaakt

#>

#
# (c) WUR
# Pas het versienummer hieronder en hierboven aan als je iets wijzigt.
# Geef bij .NOTES hierboven aan wat je precies hebt gewijzigd.
#
    [OutputType('PSCredential')]
    [CmdletBinding(
        SupportsShouldProcess = $True # Hiermee zorg je dat WhatIf en Confirm gebruikt kunnen worden :-)
    )]
    Param(
        [Parameter(Mandatory=$false,Position=1)]
        [String] $TargetName,
        [Switch] $NoWarning
    )
    BEGIN   {
        # Strict modus
        Set-StrictMode -Version Latest
        # Een versienummer van het script.
        Set-Variable versie -option Constant -value '1.5' -WhatIf:$false
        # Schrijf een melding op het scherm indien nodig (bij -Verbose)
        $scriptproperty=Write-CmdLetStartMessage -Version $versie

        $Warn=$true
        if ( $NoWarning ) { $Warn = $false }

    } # Einde BEGIN
    PROCESS {
        [PsUtils.CredMan+Credential[]] $Creds = [Array]::CreateInstance([PsUtils.CredMan+Credential], 0)
        [Int] $Results = 0
        try {
            $Results = [PsUtils.CredMan]::CredEnum($TargetName, [Ref]$Creds)
        }
        catch {
            return $_
        }
        switch($Results) {
            0           {   ForEach ( $cred in $Creds) {
                                if ( $cred.CredentialBlobSize -eq 0 ) {
                                    if ( $Warn ) {
                                        Write-Warning "Kan geen PSCredential maken, want $($cred.TargetName) heeft een leeg wachtwoord."
                                    }
                                } else {
                                    if ( $cred.UserName -eq $null ) {
                                        if ( $Warn ) {
                                            Write-Warning "Kan geen PSCredential maken, want $($cred.TargetName) heeft een lege username."
                                        }
                                    } else {
                                        # Als het al een securestring is, dan wordt deze gewoon erin gezet.
                                        $is_securestring=$true
                                        try {
                                            ConvertTo-SecureString $cred.CredentialBlob -ErrorAction Stop|Out-Null
                                        }
                                        catch {
                                            $is_securestring=$false
                                        }
                                        finally {
                                            if ( $is_securestring ) {
                                                $cred_secure=(ConvertTo-SecureString $cred.CredentialBlob)
                                            } else {
                                                $cred_secure=(ConvertTo-SecureString $cred.CredentialBlob -AsPlainText -Force)
                                            }
                                            New-Object System.Management.Automation.PSCredential ($cred.UserName, $cred_secure)
                                        }
                                    }
                                }
                            }
                            break }
            0x80070490  { break } #ERROR_NOT_FOUND
            default {
                [String] $Msg = "Kon geen credentials opvragen uit de credentialstore voor '$Env:UserName'"
                [Management.ManagementException] $MgmtException = New-Object Management.ManagementException($Msg)
                [Management.Automation.ErrorRecord] $ErrRcd = New-Object Management.Automation.ErrorRecord($MgmtException, $Results.ToString("X"), $ErrorCategory[$Results], $null)
                #$ErrRcd
            }
        }

    } # Einde PROCESS
    END     {
        # Schrijft een melding op het scherm indien nodig (bij -Verbose)
        Write-CmdLetStopMessage -ScriptProperty $scriptproperty
    } # Einde END
} # Einde Function

Function Update-Credential {
<#
.SYNOPSIS
Geeft een lijst van Credentials die je kunt updaten (interactief).

.DESCRIPTION
Geeft een lijst van Credentials die je kunt updaten (interactief).

.PARAMETER TargetName
De naam van de credential.

.EXAMPLE
Update-Credential -TargetName 'EFSA' -Credential (Get-Credential)
Haalt de credential gegevens op van 'EFSA', en werkt ze bij met de
opgegeven credentials.

.INPUTS

.OUTPUTS
[System.Management.Automation.PSCredential]

.NOTES
    Naam:           Update-Credential
    Auteur:         HH
    Bron:
    Wijzigingen:    14-12-2019  HH              1.0     Script geboren.

#>

#
# Pas het versienummer hieronder en hierboven aan als je iets wijzigt.
# Geef bij .NOTES hierboven aan wat je precies hebt gewijzigd.
#
    [OutputType('PSCredential')]
    [CmdletBinding(
        SupportsShouldProcess = $True # Hiermee zorg je dat WhatIf en Confirm gebruikt kunnen worden :-)
    )]
    Param(
        [Parameter(Mandatory=$false,Position=1)]
        [String] $TargetName,
        [System.Management.Automation.PSCredential] $Credential
    )
    BEGIN   {
        # Strict modus
        Set-StrictMode -Version Latest
        # Een versienummer van het script.
        Set-Variable versie -option Constant -value '1.0' -WhatIf:$false
        # Schrijf een melding op het scherm indien nodig (bij -Verbose)
        $scriptproperty=Write-CmdLetStartMessage -Version $versie

    } # Einde BEGIN
    PROCESS {
        if ($TargetName -and $Credential) {
            if ( Get-Credential -TargetName $TargetName -NoWarning ) {
                Write-Credential -TargetName $TargetName -Credential $Credential
            }
        } else {
            if ($TargetName) {
                $Credential=Get-Credential -TargetName $TargetName
                $Credential=Microsoft.PowerShell.Security\Get-credential -UserName $Credential.UserName
                Write-Credential -TargetName $TargetName -Credential $Credential
            } else {
                Write-Verbose "Tsja"
            }
        }

    } # Einde PROCESS
    END     {
        # Schrijft een melding op het scherm indien nodig (bij -Verbose)
        Write-CmdLetStopMessage -ScriptProperty $scriptproperty
    } # Einde END
} # Einde Function
