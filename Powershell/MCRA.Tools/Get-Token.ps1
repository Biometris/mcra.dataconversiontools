Function Get-Token {
<#
.SYNOPSIS
Retrieves an MCRA Token for access.

.DESCRIPTION
Retrieves an MCRA Token for access.

.PARAMETER CREDENTIAL
A Credential object.

.PARAMETER URL
The URL to which the request is made.

.EXAMPLE
Get-Token -Url 'https://mcra.test.wur.nl/Mcra90' -TargetName MCRA

.INPUTS
System.String

.NOTES
    Naam:           Get-Token
    Auteur:         Heuve081
    Bron:
    Wijzigingen:    11-03-2020  Heuve081     1.0     Script geboren.

#>

    #
    # (c) Private use
    # Pas het versienummer hieronder en hierboven aan als je iets wijzigt.
    # Geef bij .NOTES hierboven aan wat je precies hebt gewijzigd.
    #
    # Pas het Outputtype aan als je een object teruggeeft.
    [OutputType('MCRA.Token')]
    [CmdletBinding(SupportsShouldProcess = $True)]
    Param(
        [Parameter(Mandatory=$true)]
        [string]$Url,
        [Parameter(Mandatory=$true)]
        [System.Management.Automation.PSCredential]$Credential,
        [int]$ID
    )
    BEGIN   {
        # Strict modus
        Set-StrictMode -Version Latest
        # Een versienummer van het script.
        Set-Variable versie -option Constant -value '1.0' -WhatIf:$false
        # Schrijf een melding op het scherm indien nodig (bij -Verbose)
        $scriptproperty=Write-CmdLetStartMessage -Invoke $MyInvocation -Version $versie
        $ClientId = 'cbd00dfbdd0a4501bf457b52a635353f'

        $TokenUrl = $Url + '/jwtauth/token'

        $Body = @{
            'grant_type' = 'password';
            'username' = $Credential.UserName;
            'password' =  $Credential.GetNetworkCredential().Password;
            'client_id' = $ClientId }

    } # Einde BEGIN
    PROCESS {
        # Request token
        $now = Get-Date
        Write-Verbose "  [MESG ] Request token on $TokenUrl"
        Try {
            $token=Invoke-RestMethod -Uri $TokenUrl -ContentType 'application/json' -Body $body -Method POST
        } Catch {
            $token = $null
        }
        if ( [bool]$token ) {
            # Now stored in $token.access_token
            $headers = @{ 'authorization' = 'bearer '+$token.access_token }
            # Output a custom object, in which we can easily navigate.
            $secret = ConvertTo-SecureString $token.access_token -AsPlainText -Force
            [pscustomobject]@{
                'token' = $secret
                'header' = $headers
                # Expires 10 mins before actual expiration
                'expires' = $now.AddSeconds($token.expires_in).AddMinutes(-10)
                PSTypeName = 'MCRA.Token' }
             Write-Verbose "  [MESG ] Token created"
        } else {
            # [pscustomobject]@{ 'token' = ''; 'header' = @{}; PSTypeName = 'MCRA.Token' }
            Write-Warning "  [WARN ] Could not fetch a proper token"
            $token
        }
    } # Einde PROCESS
    END {
        # Schrijft een melding op het scherm indien nodig (bij -Verbose)
        Write-CmdLetStopMessage -ScriptProperty $scriptproperty
    } # Einde END
} # Einde Function
