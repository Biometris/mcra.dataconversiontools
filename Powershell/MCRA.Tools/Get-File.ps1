Function Get-File {
<#
.SYNOPSIS
Get a Datasource from the MCRA repository and save to file.

.DESCRIPTION
Get a Datasource from the MCRA repository and save to file.

.PARAMETER CREDENTIAL
A Credential object.

.PARAMETER Keyring
The name from the entry in the Windows Credential Manager.

.PARAMETER URL
The URL to which the request is made.

.PARAMETER ID
The ID of the repository.

.PARAMETER Path
The name to which to save the repository.

.PARAMETER AsCSV
Wil retrieve the data from the database as a ZIPPED CSV.

.EXAMPLE
Get-File


.INPUTS
System.String

.NOTES
    Naam:           Get-File
    Auteur:         Hansvandenheuvel
    Bron:
    Wijzigingen:    12-03-2020  Hansvandenheuvel     1.0     Script geboren.

#>

    #
    # (c) Private use
    # Pas het versienummer hieronder en hierboven aan als je iets wijzigt.
    # Geef bij .NOTES hierboven aan wat je precies hebt gewijzigd.
    #
    # Pas het Outputtype aan als je een object teruggeeft.
    [OutputType('My.Object')]
    [CmdletBinding(SupportsShouldProcess = $True)]
    Param(
        [Parameter(Mandatory=$true)]
        [string]$Url,
        [Parameter(Mandatory=$true,ParameterSetName='Credential')]
        [System.Management.Automation.PSCredential]$Credential,
        [Parameter(Mandatory=$true,ParameterSetName='TargetName')]
        [Alias('Keyring')]
        [string]$TargetName,
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName)]
        [Alias('ID')]
        [int]$idCurrentVersion,
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName)]
        [Alias('Path')]
        [string]$Name,
        [Parameter()]
        [switch]$AsCSV

    )
    BEGIN   {
        # Strict modus
        Set-StrictMode -Version Latest
        # Een versienummer van het script.
        Set-Variable versie -option Constant -value '1.0' -WhatIf:$false
        # Schrijf een melding op het scherm indien nodig (bij -Verbose)
        $scriptproperty=Write-CmdLetStartMessage -Invoke $MyInvocation -Version $versie

        $tokenparams= @{ 'Url' = $Url }
        if ($PSCmdlet.ParameterSetName -eq 'TargetName') {
            $tokenparams['Credential'] = Get-Credential -TargetName $TargetName
        } else {
            $tokenparams['Credential'] = $Credential
        }

        $token=Get-Token @tokenparams

        if (-not [bool]$token ) {
            Throw "  [ERROR] No token received. Mission abort."
        }

    } # Einde BEGIN
    PROCESS {
        ForEach-Object {
            if ($AsCSV) {
                # Force .zip as extension
                if ([System.IO.Path]::GetExtension($Name).ToLower() -ne '.zip') {
                    $Name=$Name+'.zip'
                }
                $Api='DataSources/DownloadVersionCsv/'+$idCurrentVersion
            } else {
                $Api='DataSources/DownloadVersion/'+$idCurrentVersion
            }
            Write-Verbose "  [MESG ] Downloading datasource with ID $idCurrentVersion to $Name"

            $RequestUrl=$Url+'/Api/'+$Api
            Invoke-WebRequest -Uri $RequestUrl -Headers $token.header -OutFile $Name
        }
    } # Einde PROCESS
    END {
        # Schrijft een melding op het scherm indien nodig (bij -Verbose)
        Write-CmdLetStopMessage -ScriptProperty $scriptproperty
    } # Einde END
} # Einde Function
